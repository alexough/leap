﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Leap.Controllers
{
    //[Authorize]
    public class LogController : Controller
    {
        //
        // GET: /Demo/

        public ActionResult Index()
        {
            LeapDataClassesDataContext context = new LeapDataClassesDataContext();
            List<leap_log> list = context.leap_logs.OrderByDescending(l => l.log_date).ToList();
            return View(list);
        }

    }
}
