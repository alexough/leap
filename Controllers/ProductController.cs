﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Leap.Controllers
{
    //[Authorize]
    public class ProductController : Controller
    {
        //
        // GET: /Product/

        public ActionResult Index(string view)
        {
            if (!User.IsInRole("Admin")) view = null;

            ViewBag.view = view;

            return View();
        }

    }
}
