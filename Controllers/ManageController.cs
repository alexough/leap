﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Leap.Controllers
{
    //[Authorize]
    public class ManageController : LogBaseController
    {
        public JsonResult Find(string name)
        {
            LeapDataClassesDataContext context = new LeapDataClassesDataContext();
            web_site_support_text support_text = Search(context, name);
            if (support_text == null)
            {
                return Json(new { error = "No text found" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(support_text, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public JsonResult Update(string name, string text)
        {
            string old_text = "";
            string new_text = text;
            LeapDataClassesDataContext context = new LeapDataClassesDataContext();
            web_site_support_text support_text = Search(context, name);
            if (support_text == null)
            {
                support_text = Create(context, name, text);
            }
            else
            {
                old_text = support_text.text_block;
                support_text = Modify(context, support_text, text);
            }

            LogPageEdit(support_text.text_block_name, old_text, new_text);

            return Json(support_text, JsonRequestBehavior.AllowGet);
        }

        private web_site_support_text Create(LeapDataClassesDataContext context, string name, string text)
        {
            web_site_support_text support_text = new web_site_support_text();
            support_text.text_block_name = name;
            support_text.text_block = text;
            context.web_site_support_texts.InsertOnSubmit(support_text);
            context.SubmitChanges();
            return support_text;
        }

        public web_site_support_text Search(LeapDataClassesDataContext context, string name)
        {
            IQueryable<web_site_support_text> query = context.web_site_support_texts.Where(w => w.text_block_name == name);

            if (query.Count() == 0)
            {
                return null;
            }

            return query.First();
        }

        private web_site_support_text Modify(LeapDataClassesDataContext context, web_site_support_text support_text, string text)
        {
            support_text.text_block = text;
            context.SubmitChanges();

            return support_text;
        }

    }
}
