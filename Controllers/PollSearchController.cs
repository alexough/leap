﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Leap.Controllers
{
    //[Authorize]
    public class PollSearchController : LogBaseController
    {

        //
        // GET: /PollSearch/

        public ActionResult Index()
        {
            LeapDataClassesDataContext context = new LeapDataClassesDataContext();
            var list = context.data_searches
                .Where(d => d.user_name == User.Identity.Name)
                .ToList();

            return View(list);
        }

        public JsonResult Find(int id)
        {
            LeapDataClassesDataContext context = new LeapDataClassesDataContext();

            IQueryable<data_search> query = context.data_searches.Where(d => d.user_name == User.Identity.Name && d.id == id);

            if (query.Count() == 0)
            {
                return Json(new { error = "No search found" }, JsonRequestBehavior.AllowGet);
            }

            var dsearch = query.ToList()
                            .Select(d => new
                                {
                                    d.id,
                                    d.user_name,
                                    d.name,
                                    d.search_string,
                                    created = d.created_datetime.ToString("yyyy-MM-dd hh:mm:ss"),
                                    modified = d.last_modified_datetime.Value.ToString("yyyy-MM-dd hh:mm:ss")
                                });

            var search = dsearch.First();
            Log("Find Search", "Search[" + search.id + ":" + search.name + "] was retrieved.");

            return Json(search, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Add(string id, string name, string search_string)
        {
            LeapDataClassesDataContext context = new LeapDataClassesDataContext();

            data_search dsearch = null;
            IQueryable<data_search> query = context.data_searches.Where(d => d.user_name == User.Identity.Name);
            if (id != null && id != "" && id != "null")
            {
                query = query.Where(d => d.id == int.Parse(id));
                if (query.Count() <= 0)
                {
                    return Json(new { error = "search with the name [" + name + "] does not exist!" }, JsonRequestBehavior.AllowGet);
                }
                dsearch = Update(context, int.Parse(id), search_string);
                Log("Update Search", "Search[" + id + ":" + name + "] was modified with search string [" + search_string + "]");
            }
            else
            {
                query = query.Where(d => d.name == name);
                if (query.Count() > 0)
                {
                    return Json(new { error = "search with the name [" + name + "] already exists!" }, JsonRequestBehavior.AllowGet);
                }
                dsearch = Create(context, name, search_string);
                Log("Create Search", "Search[" + dsearch.id + ":" + name + "] was created with search string [" + search_string + "]");
            }

            return Find(dsearch.id);
        }

        public JsonResult Delete(int id)
        {
            LeapDataClassesDataContext context = new LeapDataClassesDataContext();
            IQueryable<data_search> query = context.data_searches.Where(d => d.user_name == User.Identity.Name && d.id == id);

            if (query.Count() == 0)
            {
                return Json(new { error = "No search found" }, JsonRequestBehavior.AllowGet);
            }

            data_search dsearch = query.First();
            string search_name = dsearch.name;
            context.data_searches.DeleteOnSubmit(dsearch);
            context.SubmitChanges();
            Log("Delete Search", "Search[" + id + ":" + search_name + "] was deleted.");

            return Json(new {}, JsonRequestBehavior.AllowGet);
        }

        private data_search Create(LeapDataClassesDataContext context, string name, string search_string)
        {
            data_search dsearch = new data_search();
            dsearch.name = name;
            dsearch.user_name = User.Identity.Name;
            dsearch.created_datetime = DateTime.Now;
            dsearch.last_modified_datetime = dsearch.created_datetime;
            dsearch.search_string = search_string;
            context.data_searches.InsertOnSubmit(dsearch);
            context.SubmitChanges();
            return dsearch;
        }

        private data_search Update(LeapDataClassesDataContext context, int id, string search_string)
        {
            IQueryable<data_search> query = context.data_searches.Where(d => d.user_name == User.Identity.Name && d.id == id);

            if (query.Count() == 0)
            {
                throw new Exception("search with the id [" + id + "] does not exist!");
            }

            data_search dsearch = query.First();
            dsearch.search_string = search_string;
            dsearch.last_modified_datetime = DateTime.Now;
            context.SubmitChanges();

            return dsearch;
        }
    }
}
