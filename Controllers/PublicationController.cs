﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Leap.Controllers
{
    public class PublicationController : Controller
    {
        //
        // GET: /Publication/

        public ActionResult Index(string view)
        {
            if (!User.IsInRole("Admin")) view = null;

            ViewBag.view = view;

            return View();
        }

    }
}
