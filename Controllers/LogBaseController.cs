﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Leap.Controllers
{
    //[Authorize]
    public class LogBaseController : Controller
    {
        protected Helpers.LeapLogger leap_logger;

        public LogBaseController()
        {
            leap_logger = new Helpers.LeapLogger();
        }

        protected void Log(string method, string message)
        {
            try
            {
                LeapDataClassesDataContext context = new LeapDataClassesDataContext();
                leap_logger.Log(context, User.Identity.Name, this.Request.UserHostAddress, method, message);
            }
            catch (Exception)
            {
            }
        }

        protected void LogPageEdit(string text_block_name, string old_text, string new_text)
        {
            LeapDataClassesDataContext context = new LeapDataClassesDataContext();

            if (!User.IsInRole("Admin")) throw new Exception("You must login as an Administrator first to edit page");

            String user_name = User.Identity.Name;
            leap_logger.LogPageEdit(context, text_block_name, user_name, this.Request.UserHostAddress, old_text, new_text);
        }
    }
}
