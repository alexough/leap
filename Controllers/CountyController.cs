﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.XPath;

namespace Leap.Controllers
{
    //[Authorize]
    public class CountyController : Controller
    {
        public JsonResult Index(int state_fips, string offices, string ranges)
        {
            // build offices clauses
            var predicate_offices = PredicateBuilder.True<ldc_results_election>();
            if (offices != "")
            {
                string[] office_array = offices.Split(',');
                predicate_offices = PredicateBuilder.False<ldc_results_election>();
                foreach (string office_str in office_array)
                {
                    if (office_str == "-1")
                    {
                        predicate_offices = PredicateBuilder.True<ldc_results_election>();
                        break;
                    }
                    if (office_str == "") continue;
                    predicate_offices = predicate_offices.Or(e => e.leap_office_name_id == int.Parse(office_str));
                }
            }

            // build date clauses
            var predicate_time = PredicateBuilder.True<ldc_results_election>();
            if (ranges != "")
            {
                string[] range_array = ranges.Split(',');
                predicate_time = PredicateBuilder.False<ldc_results_election>();
                foreach (string range_str in range_array)
                {
                    if (range_str == "") continue;
                    string[] time_strs = range_str.Split(':');
                    if (time_strs[0] != "" && time_strs[1] != "")
                    {
                        //DateTime from_date = new DateTime(1970, 1, 1).AddTicks(long.Parse(time_strs[0]) * 10000);
                        //DateTime to_date = new DateTime(1970, 1, 1).AddTicks(long.Parse(time_strs[1]) * 10000);
                        DateTime from_date = DateTime.Parse(time_strs[0]);
                        DateTime to_date = DateTime.Parse(time_strs[1]);
                        predicate_time = predicate_time.Or(e => e.election_date.Value >= from_date
                            && e.election_date.Value <= to_date);
                    }
                    else if (time_strs[0] != "")
                    {
                        //DateTime from_date = new DateTime(1970, 1, 1).AddTicks(long.Parse(time_strs[0]) * 10000);
                        //DateTime to_date = new DateTime(1970, 1, 1).AddTicks(long.Parse(time_strs[1]) * 10000);
                        DateTime from_date = DateTime.Parse(time_strs[0]);
                        predicate_time = predicate_time.Or(e => e.election_date.Value >= from_date);
                    }
                    else if (time_strs[1] != "")
                    {
                        //DateTime from_date = new DateTime(1970, 1, 1).AddTicks(long.Parse(time_strs[0]) * 10000);
                        //DateTime to_date = new DateTime(1970, 1, 1).AddTicks(long.Parse(time_strs[1]) * 10000);
                        DateTime to_date = DateTime.Parse(time_strs[1]);
                        predicate_time = predicate_time.Or(e => e.election_date.Value <= to_date);
                    }
                }
            }

            LeapDataClassesDataContext context = new LeapDataClassesDataContext();
            //context.Log = Console.Out;

            List<Leap.Models.County> list = context.ldc_results_elections
                .Where(e => e.archive_flag == 0)
                .Where(e => e.state_fips == state_fips)
                .Where(predicate_offices)
                .Where(predicate_time)
                .Where(e => e.leap_office_name_id > 0)
                //.Where(e => e.government_level_id > 2)
                .Select(e => new Leap.Models.County
                {
                    StateFips = e.state_fips,
                    StateName = e.list_loc_key.state_name,
                    CountyFips = e.county_fips,
                    CountyName = e.list_loc_key.county_name
                })
                .Distinct().OrderBy(c => c.CountyName).ToList();

            //string sql = context.GetCommand(query).CommandText;

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        private List<int> FindCountiesWithData(int state_fips)
        {
            LeapDataClassesDataContext context = new LeapDataClassesDataContext();

            List<int?> list = context.ldc_results_elections
                .Where(e => e.archive_flag == 0)
                .Where(e => e.state_fips == state_fips)
                .Where(e => e.leap_office_name_id > 0)
                //.Where(e => e.government_level_id > 2)
                .Where(e => e.meta_data_log.data_type >= 0)
                .Select(e => e.county_fips)
                .Distinct().ToList();

            List<int> fips_list = new List<int>();
            foreach (int fips in list)
            {
                if (fips == 0) continue;
                fips_list.Add(fips);
            }
            return fips_list;
        }

        /*private List<int?> FindCounties(int state_fips)
        {
            LeapDataClassesDataContext context = new LeapDataClassesDataContext();

            List<int?> fips_list = context.ldc_results_elections
                .Where(e => e.archive_flag == 0)
                .Where(e => e.state_fips == state_fips)
                .Where(e => e.county_fips != null)
                .Select(e => e.county_fips)
                .Distinct().ToList();

            return fips_list;
        }*/

        public ActionResult Find(int state_fips)
        {
            List<int> county_fips_list = FindCountiesWithData(state_fips);
            String state_fips_str = state_fips.ToString("00");

            XmlDocument xml = new XmlDocument();
            String fileName = Request.PhysicalPath + "/../../../Scripts/gz_2010_us_050_00_20m.xml";
            xml.Load(fileName);

            XmlDocument outXml = new XmlDocument();
            String outfileName = Request.PhysicalPath + "/../../../Scripts/county_skeleton.xml";
            outXml.Load(outfileName);
            XmlNode outFolderNode = outXml.SelectSingleNode("/kml/Document/Folder");

            int count = 0;
            foreach (XmlNode folderNode in xml.SelectNodes("/kml/Document/Folder"))
            {
                //foreach(XmlNode xn in folderNode.SelectNodes("Placemark"))
                for(int xni = folderNode.SelectNodes("Placemark").Count-1; xni >= 0; xni--)
                {
                    XmlNode xn = folderNode.SelectNodes("Placemark")[xni];
                    if (xn["ExtendedData"].GetElementsByTagName("SchemaData")[0].SelectSingleNode("SimpleData[@name='STATE']").InnerText != state_fips_str)
                    {
                        //folderNode.RemoveChild(xn);
                    }
                    else
                    {
                        bool has_data = false;
                        string county_fips = xn["ExtendedData"].GetElementsByTagName("SchemaData")[0].SelectSingleNode("SimpleData[@name='COUNTY']").InnerText;
                        foreach (int next_fips in county_fips_list)
                        {
                            if (next_fips.ToString("000") == county_fips)
                            {
                                has_data = true;
                                break;
                            }
                        }
                        //if (!has_data) continue;

                        XmlElement element = xml.CreateElement("has_data");
                        element.AppendChild(xml.CreateTextNode(has_data.ToString()));
                        xn.AppendChild(element);

                        count++;
                        XmlNode copiedNode = outXml.ImportNode(xn, true);
                        outFolderNode.AppendChild(copiedNode);
                    }
                }
            }

            //return new Leap.Models.XmlResult(xml);
            return new Leap.Models.XmlResult(outXml);
        }

    }
}
