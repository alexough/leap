﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Leap.Controllers
{
    //[Authorize]
    public class PageEditLogController : Controller
    {
        //
        // GET: /Demo/

        public ActionResult Index()
        {
            LeapDataClassesDataContext context = new LeapDataClassesDataContext();
            List<leap_page_edit_log> list = context.leap_page_edit_logs.OrderByDescending(l => l.modified_time).ToList();
            return View(list);
        }

    }
}
