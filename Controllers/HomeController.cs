﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Leap.Controllers
{
    //[Authorize]
    public class HomeController : Controller
    {
        public ActionResult Index(string view)
        {
            if (!User.IsInRole("Admin")) view = null;

            ViewBag.view = view;

            return View();
        }

        public ActionResult About(string view)
        {
            if (!User.IsInRole("Admin")) view = null;

            ViewBag.view = view;

            return View();
        }

        public ActionResult Contact(string view)
        {
            if (!User.IsInRole("Admin")) view = null;

            ViewBag.view = view;

            return View();
        }

        public ActionResult Upload(String folderName, String resourceName)
        {
            ViewBag.folderName = folderName;
            ViewBag.resourceName = resourceName;
            return View();
        }

        [HttpPost]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Upload(String folderName, String resourceName, IEnumerable<HttpPostedFileBase> file)
        {
            if (!User.IsInRole("Admin")) throw new Exception("You must login as an Administrator first to edit page");

            ViewBag.fileName = Request.Files[0].FileName;
            ViewBag.message = "";

            string extName = ".png";
            int index = Request.Files[0].FileName.LastIndexOf(".");
            if (index > 0)
            {
                extName = Request.Files[0].FileName.Substring(index);
            }
            string fileName = resourceName + extName;

            string dir = Request.PhysicalApplicationPath + "\\" + folderName;
            Request.Files[0].SaveAs(dir + "\\" + fileName);

            ViewBag.message = "Resource File [" + fileName + "] has been successfully uploaded.";

            return View();
        }
    }
}
