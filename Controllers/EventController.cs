﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Leap.Controllers
{
    public class EventController : LogBaseController
    {
        //
        // GET: /Poll/

        public ActionResult Index(string num_to_fetch, string view)
        {
            if (!User.IsInRole("Admin")) view = null;

            ViewBag.view = view;
            
            LeapDataClassesDataContext context = new LeapDataClassesDataContext();
            List<leap_event> list = new List<leap_event>();
            var query = context.leap_events.OrderByDescending(e => e.date);
            if (num_to_fetch != null)
            {
                list = query.Take(int.Parse(num_to_fetch)).ToList();
            }
            else
            {
                list = query.ToList();
            }

            List<Models.EventYearArchive> year_archive_list = find_archives_in_years(context);
            foreach (Models.EventYearArchive year_archive in year_archive_list)
            {
                List<Models.EventMonthArchive> month_archive_list = find_archives_in_months(context, year_archive.Year);
                year_archive.MonthArchiveList = month_archive_list;
            }
            ViewBag.archives = year_archive_list;

            return View(list);
        }

        public ActionResult FindAll(int num_to_fetch, string view, string page)
        {
            if (!User.IsInRole("Admin")) view = null;

            ViewBag.view = view;
            ViewBag.page = page;

            LeapDataClassesDataContext context = new LeapDataClassesDataContext();
            List<leap_event> list = context.leap_events.OrderByDescending(e => e.date).Take(num_to_fetch).ToList();
            return View(list);
        }

        public ActionResult Find(int id)
        {
            LeapDataClassesDataContext context = new LeapDataClassesDataContext();
            leap_event l_event = Search(context, id);

            List<Models.EventYearArchive> year_archive_list = find_archives_in_years(context);
            foreach (Models.EventYearArchive year_archive in year_archive_list)
            {
                List<Models.EventMonthArchive> month_archive_list = find_archives_in_months(context, year_archive.Year);
                year_archive.MonthArchiveList = month_archive_list;
            }
            ViewBag.archives = year_archive_list;

            if (l_event == null)
            {
                return View();
            }
            else
            {
                return View(l_event);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public JsonResult Update(string id, DateTime date, string author, string summary, string detail)
        {
            string event_id = "news_new";
            string old_text = "";
            string new_text = ToStr(date, author, summary, detail);
            LeapDataClassesDataContext context = new LeapDataClassesDataContext();
            leap_event l_event = Search(context, int.Parse(id));
            if (l_event == null)
            {
                l_event = Create(context, date, author, summary, detail);
            }
            else
            {
                event_id = "news_" + id;
                old_text = ToStr(l_event.date, l_event.author, l_event.summary, l_event.detail);
                l_event = Modify(context, l_event, date, author, summary, detail);
            }

            LogPageEdit(event_id, old_text, new_text);

            return Json(l_event, JsonRequestBehavior.AllowGet);
        }

        public leap_event Search(LeapDataClassesDataContext context, int id)
        {
            if (id < 0) return null;

            IQueryable<leap_event> query = context.leap_events.Where(e => e.id == id);

            if (query.Count() == 0)
            {
                return null;
            }

            return query.First();
        }

        private leap_event Create(LeapDataClassesDataContext context, DateTime date, string author, string summary, string detail)
        {
            leap_event l_event = new leap_event();
            l_event.date = date;
            l_event.author = author;
            l_event.summary = summary;
            l_event.detail = detail;
            context.leap_events.InsertOnSubmit(l_event);
            context.SubmitChanges();
            return l_event;
        }

        private leap_event Modify(LeapDataClassesDataContext context, leap_event l_event, DateTime date, string author, string summary, string detail)
        {
            l_event.date = date;
            l_event.author = author;
            l_event.summary = summary;
            l_event.detail = detail;
            context.SubmitChanges();

            return Search(context, l_event.id);
        }

        private List<Models.EventYearArchive> find_archives_in_years(LeapDataClassesDataContext context)
        {
            //context.leap_events.GroupBy(e => new {e.date.Year, e.date.Month}).Select(g => new { g.Key, count = g.Count(e => e.summary)});
            return context.leap_events.GroupBy(e => e.date.Year)
                .Select(g => new Models.EventYearArchive
                    { Year = g.Key, Count = g.Count() }
                 ).OrderByDescending(a => a.Year).ToList();
        }

        private List<Models.EventMonthArchive> find_archives_in_months(LeapDataClassesDataContext context, int year)
        {
            List<Models.EventMonthArchive> list = context.leap_events.Where(e => e.date.Year == year).GroupBy(e => e.date.Month)
                .Select(g => new Models.EventMonthArchive
                    { Year = year, Month = g.Key, Count = g.Count() }
                 ).OrderByDescending(a => a.Month).ToList();
            foreach (Models.EventMonthArchive archive in list)
            {
                archive.MonthInStr = new DateTime(archive.Year, archive.Month, 1).ToString("MMMM");
            }

            return list;
        }

        private string ToStr(DateTime date, string author, string summary, string detail)
        {
            string str = "";
            str += "[DATE]" + date.ToString("yyyy-MM-dd hh:mm:ss");
            str += "[AUTHOR]" + author;
            str += "[SUMMARY]" + summary;
            str += "[DETAIL]" + detail;
            return str;
        }
    }
}
