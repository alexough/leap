﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Leap.Controllers
{
    //[Authorize]
    public class StateController : Controller
    {
        public JsonResult Index(int country, string offices, string ranges)
        {
            // build offices clauses
            var predicate_offices = PredicateBuilder.True<ldc_results_election>();
            if (offices != "")
            {
                string[] office_array = offices.Split(',');
                predicate_offices = PredicateBuilder.False<ldc_results_election>();
                foreach (string office_str in office_array)
                {
                    if (office_str == "-1")
                    {
                        predicate_offices = PredicateBuilder.True<ldc_results_election>();
                        break;
                    }
                    if (office_str == "" ) continue;
                    predicate_offices = predicate_offices.Or(e => e.leap_office_name_id == int.Parse(office_str));
                }
            }

            // build office clauses
            var predicate_time = PredicateBuilder.True<ldc_results_election>();
            if (ranges != "")
            {
                string[] range_array = ranges.Split(',');
                predicate_time = PredicateBuilder.False<ldc_results_election>();
                foreach (string range_str in range_array)
                {
                    if (range_str == "") continue;
                    string[] time_strs = range_str.Split(':');
                    if (time_strs[0] != "" && time_strs[1] != "")
                    {
                        //DateTime from_date = new DateTime(1970, 1, 1).AddTicks(long.Parse(time_strs[0]) * 10000);
                        //DateTime to_date = new DateTime(1970, 1, 1).AddTicks(long.Parse(time_strs[1]) * 10000);
                        DateTime from_date = DateTime.Parse(time_strs[0]);
                        DateTime to_date = DateTime.Parse(time_strs[1]);
                        predicate_time = predicate_time.Or(e => e.election_date.Value >= from_date
                            && e.election_date.Value <= to_date);
                    }
                    else if (time_strs[0] != "")
                    {
                        //DateTime from_date = new DateTime(1970, 1, 1).AddTicks(long.Parse(time_strs[0]) * 10000);
                        //DateTime to_date = new DateTime(1970, 1, 1).AddTicks(long.Parse(time_strs[1]) * 10000);
                        DateTime from_date = DateTime.Parse(time_strs[0]);
                        predicate_time = predicate_time.Or(e => e.election_date.Value >= from_date);
                    }
                    else if (time_strs[1] != "")
                    {
                        //DateTime from_date = new DateTime(1970, 1, 1).AddTicks(long.Parse(time_strs[0]) * 10000);
                        //DateTime to_date = new DateTime(1970, 1, 1).AddTicks(long.Parse(time_strs[1]) * 10000);
                        DateTime to_date = DateTime.Parse(time_strs[1]);
                        predicate_time = predicate_time.Or(e => e.election_date.Value <= to_date);
                    }
                }
            }

            LeapDataClassesDataContext context = new LeapDataClassesDataContext();

            List<Leap.Models.State> list = context.ldc_results_elections
                .Where(e => e.archive_flag == 0)
                .Where(e => e.country_id == country)
                .Where(predicate_offices)
                .Where(predicate_time)
                .Where(e => e.leap_office_name_id > 0)
                //.Where(e => e.government_level_id > 2)
                .Select(e => new Leap.Models.State
                {
                    CountryId = e.country_id,
                    StateFips = e.state_fips,
                    StateName = e.list_loc_key.state_name,
                    StateAbbrev = e.list_loc_key.state_abbrev
                })
                .Distinct().OrderBy(s => s.StateName).ToList();

            return Json(list, JsonRequestBehavior.AllowGet);
        }
    }
}
