﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.XPath;

namespace Leap.Controllers
{
    [Authorize]
    public class SchoolDistrictController : Controller
    {
        //
        // GET: /SchoolDistrict/

        private List<int> FindDistrictsWithData(int state_fips)
        {
            LeapDataClassesDataContext context = new LeapDataClassesDataContext();

            List<int?> list = context.ldc_results_elections
                .Where(e => e.archive_flag == 0)
                .Where(e => e.state_fips == state_fips)
                .Where(e => e.leap_office_name_id > 0)
                //.Where(e => e.government_level_id > 2)
                .Where(e => e.meta_data_log.data_type == 1)
                .Select(e => e.entity_code)
                .Distinct().ToList();

            List<int> entity_list = new List<int>();
            foreach (int entity in list)
            {
                if (entity == 0) continue;
                entity_list.Add(entity);
            }
            return entity_list;
        }

        public JsonResult Find(int state_fips)
        {
            LeapDataClassesDataContext context = new LeapDataClassesDataContext();

            var entity_list = context.ldc_results_elections
                .Where(e => e.archive_flag == 0)
                .Where(e => e.state_fips == state_fips)
                .Where(e => e.leap_office_name_id > 0)
                //.Where(e => e.government_level_id > 2)
                .Where(e => e.meta_data_log.data_type == 1)
                .Select(e => e.entity_code)
                .Distinct();

            var list = context.list_fips_meta_datas
                .Where(m => m.state_fips == state_fips)
                .Where(m => entity_list.Contains(m.entity_code))
                .Where(m => m.primary_latitude != 0 && m.primary_longitude != 0)
                .Select(m => new { m.state_fips, m.entity_code, m.primary_latitude, m.primary_longitude })
                .Distinct()
                .OrderBy(m => m.entity_code)
                .ToList();

            return Json(list, JsonRequestBehavior.AllowGet);
        }
    }
}
