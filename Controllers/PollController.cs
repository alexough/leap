﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using System.IO;
using System.Xml.Xsl;


namespace Leap.Controllers
{
    //[Authorize]
    public class PollController : LogBaseController
    {
        //
        // GET: /Poll/

        int NUM_OF_SAMPLE_RECORDS = 10;
        int NUM_OF_RECORDS_IN_PAGES = 500;

        [AllowAnonymous]
        public ActionResult Index(string tab, int country_id = 1, string view = null)
        {
            if (!User.IsInRole("Admin")) view = null;

            ViewBag.tab = tab;
            ViewBag.view = view;

            LeapDataClassesDataContext context = new LeapDataClassesDataContext();
            List<Leap.Models.State> list = context.list_loc_keys.Where(s => s.country_id == country_id)
                .Select(s => new Leap.Models.State
                {
                    CountryId = s.country_id,
                    StateFips = s.state_fips,
                    StateName = s.state_name,
                    StateAbbrev = s.state_abbrev
                })
                .OrderBy(s => s.StateName).ToList();
            return View(list);
        }

        [AllowAnonymous]
        public ActionResult Download(string tab, int country_id = 1, string view = null)
        {
            if (!User.IsInRole("Admin")) view = null;

            ViewBag.tab = tab;
            ViewBag.view = view;

            LeapDataClassesDataContext context = new LeapDataClassesDataContext();
            List<Leap.Models.State> list = context.list_loc_keys.Where(s => s.country_id == country_id)
                .Select(s => new Leap.Models.State
                {
                    CountryId = s.country_id,
                    StateFips = s.state_fips,
                    StateName = s.state_name,
                    StateAbbrev = s.state_abbrev
                })
                .OrderBy(s => s.StateName).ToList();
            return View(list);
        }

        [AllowAnonymous]
        public JsonResult AllOfficeNamesInJson(string counties, string ranges)
        {
            var predicate_fips = PredicateBuilder.True<ldc_results_election>();
            if (counties != "")
            {
                string[] fips_array = counties.Split(',');
                predicate_fips = PredicateBuilder.False<ldc_results_election>();
                foreach (string fip_str in fips_array)
                {
                    if (fip_str == "") continue;
                    string[] state_county_fips = fip_str.Split(':');
                    if (state_county_fips[1] == "" || state_county_fips[1] == "-1")
                    {
                        predicate_fips = predicate_fips.Or(e => e.state_fips == int.Parse(state_county_fips[0]));
                    }
                    else
                    {
                        predicate_fips = predicate_fips.Or(e => e.state_fips == int.Parse(state_county_fips[0])
                            && e.county_fips == int.Parse(state_county_fips[1]));
                    }
                }
            }

            // build office clauses
            var predicate_time = PredicateBuilder.True<ldc_results_election>();
            if (ranges != "")
            {
                string[] range_array = ranges.Split(',');
                predicate_time = PredicateBuilder.False<ldc_results_election>();
                foreach (string range_str in range_array)
                {
                    if (range_str == "") continue;
                    string[] time_strs = range_str.Split(':');
                    if (time_strs[0] != "" && time_strs[1] != "")
                    {
                        DateTime from_date = DateTime.Parse(time_strs[0]);
                        DateTime to_date = DateTime.Parse(time_strs[1]);
                        predicate_time = predicate_time.Or(e => e.election_date.Value >= from_date
                            && e.election_date.Value <= to_date);
                    }
                    else if (time_strs[0] != "")
                    {
                        DateTime from_date = DateTime.Parse(time_strs[0]);
                        predicate_time = predicate_time.Or(e => e.election_date.Value >= from_date);
                    }
                    else if (time_strs[1] != "")
                    {
                        DateTime to_date = DateTime.Parse(time_strs[1]);
                        predicate_time = predicate_time.Or(e => e.election_date.Value <= to_date);
                    }
                }
            }

            LeapDataClassesDataContext context = new LeapDataClassesDataContext();
            var list = context.ldc_results_elections
                .Where(e => e.archive_flag == 0)
                .Where(predicate_fips)
                .Where(predicate_time)
                .Where(e => e.leap_office_name_id > 0)
                .Select(e => new
                    {
                        e.leap_office_name_id,
                        e.list_leap_office_name.leap_office_name
                    })
                .Distinct()
                .OrderBy(e => e.leap_office_name)
                .ToList();

            List<Leap.Models.OfficeName> office_names = new List<Leap.Models.OfficeName>();
            foreach (var office in list)
            {
                Leap.Models.OfficeName office_name = new Leap.Models.OfficeName();
                office_name.Id = office.leap_office_name_id.Value;
                office_name.Name = office.leap_office_name;
                office_name.Synonyms = context.list_leap_office_name_synonyms
                    .Where(s => s.leap_office_name_id == office.leap_office_name_id)
                    .OrderBy(s => s.office_name_synonym)
                    .Select(s => s.office_name_synonym )
                    .Distinct().ToList();
                office_names.Add(office_name);
            }
            return Json(office_names, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public JsonResult AllDatesInJson(string counties, string offices)
        {
            var predicate_fips = PredicateBuilder.True<ldc_results_election>();
            if (counties != "")
            {
                string[] fips_array = counties.Split(',');
                predicate_fips = PredicateBuilder.False<ldc_results_election>();
                foreach (string fip_str in fips_array)
                {
                    if (fip_str == "") continue;
                    string[] state_county_fips = fip_str.Split(':');
                    if (state_county_fips[1] == "" || state_county_fips[1] == "-1")
                    {
                        predicate_fips = predicate_fips.Or(e => e.state_fips == int.Parse(state_county_fips[0]));
                    }
                    else
                    {
                        predicate_fips = predicate_fips.Or(e => e.state_fips == int.Parse(state_county_fips[0])
                            && e.county_fips == int.Parse(state_county_fips[1]));
                    }
                }
            }

            // build offices clauses
            var predicate_offices = PredicateBuilder.True<ldc_results_election>();
            if (offices != "")
            {
                string[] office_array = offices.Split(',');
                predicate_offices = PredicateBuilder.False<ldc_results_election>();
                foreach (string office_str in office_array)
                {
                    if (office_str == "-1")
                    {
                        predicate_offices = PredicateBuilder.True<ldc_results_election>();
                        break;
                    }
                    if (office_str == "") continue;
                    predicate_offices = predicate_offices.Or(e => e.leap_office_name_id == int.Parse(office_str));
                }
            }

            LeapDataClassesDataContext context = new LeapDataClassesDataContext();
            var query = context.ldc_results_elections
                .Where(e => e.archive_flag == 0)
                .Where(predicate_fips)
                .Where(predicate_offices)
                .Where(e => e.leap_office_name_id > 0)
                .Select(e => new { e.election_date })
                .Distinct()
                .OrderBy(e => e.election_date);

            string sql = context.GetCommand(query).CommandText;

            var list = query.ToList();

            List<string> dates = new List<string>();
            for (int idx = 0; idx < list.Count; idx++)
            {
                dates.Add(list[idx].election_date.Value.ToString("yyyy-MM-dd"));
            }
            return Json(dates, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public JsonResult AllInJson(int country_id = 1)
        {
            LeapDataClassesDataContext context = new LeapDataClassesDataContext();
            List<Leap.Models.State> list = context.list_loc_keys.Where(s => s.country_id == country_id)
                .Select(s => new Leap.Models.State
                {
                    StateFips = s.state_fips,
                    StateName = s.state_name,
                    StateAbbrev = s.state_abbrev
                })
                .OrderBy(s => s.StateName).ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public ActionResult Find(int? s, string fips, string office, string time, Boolean referendums)
        {
            int start = 0;
            if (s != null) start = s.Value;

            ViewBag.start_index = start;
            ViewBag.fips = fips;
            ViewBag.office = office;
            ViewBag.time = time;
            ViewBag.referendums = referendums.ToString();

            return View();
        }

        [AllowAnonymous]
        public ActionResult Show(int start, string fips, string office, string time, Boolean referendums)
        {
            int num_rows = NUM_OF_RECORDS_IN_PAGES;
            try
            {
                num_rows = int.Parse(System.Configuration.ConfigurationManager.AppSettings["num_of_records_in_a_page"]);
            }
            catch(Exception)
            {
            }

            IQueryable<ldc_results_election_candidate> list = FindElectionCandidateList(fips, office, time, referendums);
            ViewBag.count = list.Count();
            ViewBag.fips = fips;
            ViewBag.office = office;
            ViewBag.time = time;
            ViewBag.referendums = referendums.ToString();

            Log("Preview", "Fips[" + fips + "], Offices[" + office + "], Times[" + time + "]");

            if (User.IsInRole("Admin") || User.IsInRole("Guest"))
            {
                ViewBag.is_admin = "true";
                ViewBag.start_index = start;
                ViewBag.num_rows = num_rows;
                return View(list.Skip(start).Take(num_rows).ToList());
            }
            else
            {
                int preview_num_of_records = NUM_OF_SAMPLE_RECORDS;
                try
                {
                    preview_num_of_records = int.Parse(System.Configuration.ConfigurationManager.AppSettings["preview_num_of_records"]);
                }
                catch(Exception)
                {
                }
                ViewBag.is_admin = "false";
                ViewBag.start_index = 0;
                ViewBag.num_rows = preview_num_of_records;
                return View(list.Take(preview_num_of_records).ToList());
            }
        }

        [AllowAnonymous]
        public ActionResult ShowByGroup(string counties, string offices, string ranges, Boolean referendums)
        {
            var predicate_fips = PredicateBuilder.True<ldc_results_election>();
            if (counties != "")
            {
                string[] fips_array = counties.Split(',');
                predicate_fips = PredicateBuilder.False<ldc_results_election>();
                foreach (string fip_str in fips_array)
                {
                    if (fip_str == "") continue;
                    string[] state_county_fips = fip_str.Split(':');
                    if (state_county_fips[1] == "" || state_county_fips[1] == "-1")
                    {
                        predicate_fips = predicate_fips.Or(e => e.state_fips == int.Parse(state_county_fips[0]));
                    }
                    else
                    {
                        predicate_fips = predicate_fips.Or(e => e.state_fips == int.Parse(state_county_fips[0])
                            && e.county_fips == int.Parse(state_county_fips[1]));
                    }
                }
            }

            // build offices clauses
            var predicate_offices = PredicateBuilder.True<ldc_results_election>();
            if (offices != "")
            {
                string[] office_array = offices.Split(',');
                predicate_offices = PredicateBuilder.False<ldc_results_election>();
                foreach (string office_str in office_array)
                {
                    if (office_str == "-1")
                    {
                        predicate_offices = PredicateBuilder.True<ldc_results_election>();
                        break;
                    }
                    if (office_str == "") continue;
                    predicate_offices = predicate_offices.Or(e => e.leap_office_name_id == int.Parse(office_str));
                }
            }

            var predicate_time = PredicateBuilder.True<ldc_results_election>();
            if (ranges != "")
            {
                string[] range_array = ranges.Split(',');
                predicate_time = PredicateBuilder.False<ldc_results_election>();
                foreach (string range_str in range_array)
                {
                    if (range_str == "") continue;
                    string[] time_strs = range_str.Split(':');
                    if (time_strs[0] != "" && time_strs[1] != "")
                    {
                        DateTime from_date = DateTime.Parse(time_strs[0]);
                        DateTime to_date = DateTime.Parse(time_strs[1]);
                        predicate_time = predicate_time.Or(e => e.election_date.Value >= from_date
                            && e.election_date.Value <= to_date);
                    }
                    else if (time_strs[0] != "")
                    {
                        DateTime from_date = DateTime.Parse(time_strs[0]);
                        predicate_time = predicate_time.Or(e => e.election_date.Value >= from_date);
                    }
                    else if (time_strs[1] != "")
                    {
                        DateTime to_date = DateTime.Parse(time_strs[1]);
                        predicate_time = predicate_time.Or(e => e.election_date.Value <= to_date);
                    }
                }
            }

            LeapDataClassesDataContext context = new LeapDataClassesDataContext();
            IQueryable<ldc_results_election> query = context.ldc_results_elections
                .Where(e => e.archive_flag == 0)
                .Where(predicate_fips)
                .Where(predicate_offices)
                .Where(predicate_time);
            if (!referendums)
            {
                query = query.Where(e => e.leap_office_name_id > 0);
            }

            var list = query
                .Where(e => e.election_date != null)
                .GroupBy(e => new { e.list_loc_key.state_name, e.list_loc_key.county_name, e.list_leap_office_name.leap_office_name, e.election_date })
                .Select(e => new { e.Key.state_name, e.Key.county_name, e.Key.leap_office_name, e.Key.election_date, rdate = e.Max(el => el.ldc_results_template_run.timestamp), count = e.Count() })
                .OrderBy(e => e.state_name)
                .ThenBy(e => e.county_name)
                .ThenBy(e => e.leap_office_name)
                .ThenBy(e => e.election_date)
                .ToList()
                .Select(e => new Leap.Models.ElectionByGroup
                    {
                        StateName = e.state_name,
                        CountyName = e.county_name,
                        LeapOfficeName = e.leap_office_name,
                        ElectionDate = (e.election_date.HasValue) ? e.election_date.Value.ToString("yyyy-MM-dd") : "",
                        TemplateRunDate = (e.rdate.HasValue)? e.rdate.Value.ToString("yyyy-MM-dd") : "",
                        ElectionCount = e.count
                    });

            Log("Find Election", "Fips[" + counties + "], Offices[" + offices + "], Times[" + ranges + "]");

            return View(list.ToList());
        }

        [AllowAnonymous]
        public Leap.Helpers.ExcelResult ExportToExcel(string fips, string office, string time, Boolean referendums)
        {
            string xslPath = @"Content\xslt\list_to_csv.xslt";
            List<ldc_results_election_candidate> list = FindElectionCandidateList(fips, office, time, referendums).ToList();

            Log("Export To Excel", "Fips[" + fips + "], Offices[" + office + "], Times[" + time + "]");

            return new Leap.Helpers.ExcelResult
            {
                FileName = "PollResults.xls",
                XMLStream = BuildExcel(ToXml(list), Request.PhysicalApplicationPath + xslPath)
            };
        }

        private IQueryable<ldc_results_election_candidate> FindElectionCandidateList(string fips, string office, string time, Boolean referendums)
        {
            // build fips clauses
            var predicate_fips = PredicateBuilder.True<ldc_results_election_candidate>();
            if (fips != "")
            {
                string[] fips_array = fips.Split(',');
                predicate_fips = PredicateBuilder.False<ldc_results_election_candidate>();
                foreach (string fip_str in fips_array)
                {
                    if (fip_str == "") continue;
                    string[] state_county_fips = fip_str.Split(':');
                    if (state_county_fips[1] == "" || state_county_fips[1] == "-1")
                    {
                        predicate_fips = predicate_fips.Or(c => c.ldc_results_election.state_fips == int.Parse(state_county_fips[0]));
                    }
                    else
                    {
                        predicate_fips = predicate_fips.Or(c => c.ldc_results_election.state_fips == int.Parse(state_county_fips[0])
                            && c.ldc_results_election.county_fips == int.Parse(state_county_fips[1]));
                    }
                }
            }

            // build office clauses
            var predicate_office = PredicateBuilder.True<ldc_results_election_candidate>();
            if (office != "")
            {
                string[] office_array = office.Split(',');
                predicate_office = PredicateBuilder.False<ldc_results_election_candidate>();
                foreach (string office_str in office_array)
                {
                    if (office_str == "-1")
                    {
                        predicate_office = PredicateBuilder.True<ldc_results_election_candidate>();
                        break;
                    }
                    if (office_str == "") continue;
                    predicate_office = predicate_office.Or(c => c.ldc_results_election.leap_office_name_id == int.Parse(office_str));
                }
            }

            // build office clauses
            var predicate_time = PredicateBuilder.True<ldc_results_election_candidate>();
            if (time != "")
            {
                string[] time_array = time.Split(',');
                predicate_time = PredicateBuilder.False<ldc_results_election_candidate>();
                foreach (string time_str in time_array)
                {
                    if (time_str == "") continue;
                    string[] time_strs = time_str.Split(':');
                    if (time_strs[0] != "" && time_strs[1] != "")
                    {
                        DateTime from_date = DateTime.Parse(time_strs[0]);
                        DateTime to_date = DateTime.Parse(time_strs[1]);
                        predicate_time = predicate_time.Or(c => c.ldc_results_election.election_date.Value >= from_date
                            && c.ldc_results_election.election_date.Value <= to_date);
                    }
                    else if (time_strs[0] != "")
                    {
                        DateTime from_date = DateTime.Parse(time_strs[0]);
                        predicate_time = predicate_time.Or(c => c.ldc_results_election.election_date.Value >= from_date);
                    }
                    else if (time_strs[1] != "")
                    {
                        DateTime to_date = DateTime.Parse(time_strs[1]);
                        predicate_time = predicate_time.Or(c => c.ldc_results_election.election_date.Value <= to_date);
                    }
                }
            }

            LeapDataClassesDataContext context = new LeapDataClassesDataContext();

            IQueryable<ldc_results_election_candidate> query = context.ldc_results_election_candidates
                .Where(predicate_fips)
                .Where(predicate_office)
                .Where(predicate_time);
            if (!referendums)
            {
                query = query.Where(c => c.ldc_results_election.leap_office_name_id > 0);
            }
            query = query.OrderBy(c => c.ldc_results_election.list_loc_key.state_name)
                .ThenBy(c => c.ldc_results_election.state_fips)
                .ThenBy(c => c.ldc_results_election.election_date)
                .ThenBy(c => c.ldc_results_election.office_name);

            string sql = context.GetCommand(query).CommandText;

            return query;
        }

        private string ToXml(List<ldc_results_election_candidate> list)
        {
            XDocument document =
                new XDocument(
                    new XElement("candidates",
                        from c in list
                        select new XElement("candidate",
                            new XElement("state_fips", c.ldc_results_election.state_fips),
                            new XElement("county_fips", c.ldc_results_election.county_fips),
                            new XElement("office_name", c.ldc_results_election.office_name),
                            new XElement("election_date", c.ldc_results_election.election_date),
                            new XElement("candidate_name", c.candidate_name),
                            new XElement("candidate_total_votes", c.candidate_total_votes),
                            new XElement("candidate_pct", c.candidate_pct),
                            new XElement("candidate_win", c.candidate_win),
                            new XElement("state_abbrev", c.ldc_results_election.list_loc_key.state_abbrev),
                            new XElement("county_name", c.ldc_results_election.list_loc_key.county_name),
                            new XElement("entity_name", DisplayEntityName(c.ldc_results_election.state_fips, c.ldc_results_election.county_fips, c.ldc_results_election.entity_code)),
                            new XElement("entity_code", c.ldc_results_election.entity_code),
                            new XElement("meta_data_name", c.ldc_results_election.meta_data_log.meta_data_name),
                            new XElement("fips_lookup_error", c.ldc_results_election.fips_lookup_error),
                            new XElement("office_al", c.ldc_results_election.office_al),
                            new XElement("office_district", c.ldc_results_election.office_district),
                            new XElement("seat_name", c.ldc_results_election.seat_name),
                            new XElement("registered_votes", c.ldc_results_election.registered_votes),
                            new XElement("ballots_cast", c.ldc_results_election.ballots_cast),
                            new XElement("timestamp", c.ldc_results_election.timestamp),
                            new XElement("url", c.ldc_results_election.url),
                            new XElement("office_type_name", (c.ldc_results_election.list_office_type!=null)?c.ldc_results_election.list_office_type.office_type_name:""),
                            new XElement("office_type_id", c.ldc_results_election.office_type_id),
                            new XElement("government_level_name", (c.ldc_results_election.list_government_level!=null)?c.ldc_results_election.list_government_level.government_level_name:""),
                            new XElement("government_level_id", c.ldc_results_election.government_level_id),
                            new XElement("race_id", c.ldc_results_election.race_id),
                            new XElement("notes_count", c.ldc_results_election.notes_count),
                            new XElement("page_date", c.ldc_results_election.page_date),
                            new XElement("election_name", c.ldc_results_election.election_name),
                            new XElement("leap_office_name", (c.ldc_results_election.list_leap_office_name!=null)?c.ldc_results_election.list_leap_office_name.leap_office_name:""),
                            new XElement("leap_office_name_id", c.ldc_results_election.leap_office_name_id),
                            new XElement("election_type_desc", c.ldc_results_election.election_type_desc),
                            new XElement("election_type_referendum", DisplayElectionType(c.ldc_results_election.election_type_referendum)),
                            new XElement("election_type_primary", DisplayElectionType(c.ldc_results_election.election_type_primary)),
                            new XElement("election_type_general", DisplayElectionType(c.ldc_results_election.election_type_general)),
                            new XElement("election_type_runoff", DisplayElectionType(c.ldc_results_election.election_type_runoff)),
                            new XElement("election_type_special", DisplayElectionType(c.ldc_results_election.election_type_special)),
                            new XElement("year", c.ldc_results_election.year_int),
                            new XElement("month", c.ldc_results_election.month_int),
                            new XElement("day", c.ldc_results_election.day_int),
                            new XElement("vote_for", c.ldc_results_election.vote_for_int),
                            new XElement("candidate_first_name", c.candidate_first_name),
                            new XElement("candidate_middle_name", c.candidate_middle_name),
                            new XElement("candidate_last_name", c.candidate_last_name),
                            new XElement("candidate_nick_name", c.candidate_nick_name),
                            new XElement("candidate_name_suffix", c.candidate_name_suffix),
                            new XElement("candidate_party_id", c.candidate_party_id),
                            new XElement("candidate_name_type", c.candidate_name_type),
                            new XElement("candidate_referendum_type", c.candidate_referendum_type),
                            new XElement("candidate_incumbent", c.candidate_incumbent),
                            new XElement("candidate_occupation", c.candidate_occupation),
                            new XElement("candidate_pid", c.candidate_pid),
                            new XElement("candidate_absent_votes", c.candidate_absent_votes),
                            new XElement("candidate_absent_pct", c.candidate_absent_pct),
                            new XElement("candidate_early_votes", c.candidate_early_votes),
                            new XElement("candidate_early_pct", c.candidate_early_pct),
                            new XElement("candidate_ed_votes", c.candidate_ed_votes),
                            new XElement("candidate_ed_pct", c.candidate_ed_pct),
                            new XElement("candidate_mail_votes", c.candidate_mail_votes),
                            new XElement("candidate_mail_pct", c.candidate_mail_pct),
                            new XElement("results_election_id", c.ldc_results_election.results_election_id),
                            new XElement("template_run_id", c.ldc_results_election.template_run_id),
                            new XElement("country_id", c.ldc_results_election.country_id)
                        )
                    )
                );

            return document.ToString();
        }

        private string BuildExcel(string xml, string xslPath)
        {
            XPathDocument xpathDoc = new XPathDocument(new StringReader(xml));
            XslCompiledTransform transform = new XslCompiledTransform();
            transform.Load(xslPath);

            XsltArgumentList argsList = new XsltArgumentList();

            // Add the required parameters to the XsltArgumentList object
            //argsList.AddParam(paramName, "", paramValue);

            System.IO.StringWriter swriter = new System.IO.StringWriter();
            transform.Transform(xpathDoc, argsList, swriter);
            swriter.Flush();
            swriter.Close();
            return swriter.ToString();
        }

        private string DisplayElectionType(int? input)
        {
            if (input.HasValue)
            {
                if (input.Value == 0)
                {
                    return "no";
                }
                else
                {
                    return "yes";
                }
            }
            else
            {
                return "no";
            }
        }

        private string DisplayEntityName(int? stat_fips, int? county_fips, int? entity_code)
        {
            LeapDataClassesDataContext context = new LeapDataClassesDataContext();
            List<list_fips_meta_data> meta_data_list = context.list_fips_meta_datas
                .Where(m => m.state_fips == stat_fips)
                .Where(m => m.county_fips == county_fips)
                .Where(m => m.entity_code == entity_code).ToList();
            if (meta_data_list.Count == 0) return "";
            return meta_data_list[0].entity_name;
        }
    }
}
