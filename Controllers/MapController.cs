﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.XPath;

namespace Leap.Controllers
{
    //[Authorize]
    public class MapController : Controller
    {
        //
        // GET: /Map/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AllStateMatrix()
        {
            LeapDataClassesDataContext context = new LeapDataClassesDataContext();
            List<Leap.Models.StateMatrix> matrix_list = new List<Models.StateMatrix>();

            foreach (int state_fips in FindStatesWithData())
            {
                matrix_list.Add(FindStateMatrix(context, state_fips));
            }

            return View(matrix_list);
        }

        public ActionResult Matrix(string fips)
        {
            int state_fips = int.Parse(fips);

            LeapDataClassesDataContext context = new LeapDataClassesDataContext();
            Leap.Models.StateMatrix matrix = FindStateMatrix(context, state_fips);
            return View(matrix);
        }

        public ActionResult MatrixElectionDetail(string fips, string gid)
        {
            int state_fips = int.Parse(fips);
            int government_level_id = int.Parse(gid);
            LeapDataClassesDataContext context = new LeapDataClassesDataContext();
            List<ldc_results_election> election_list = GetElectionList(context, state_fips, government_level_id);
            return View(election_list);
        }

        public ActionResult MatrixPlaceDetail(string fips, string gid)
        {
            int state_fips = int.Parse(fips);
            int government_level_id = int.Parse(gid);
            LeapDataClassesDataContext context = new LeapDataClassesDataContext();
            List<ldc_results_election> place_list = GetPlaceList(context, state_fips, government_level_id);
            return View(place_list);
        }

        public ActionResult MatrixCandidateDetail(string fips, string gid)
        {
            int state_fips = int.Parse(fips);
            int government_level_id = int.Parse(gid);
            LeapDataClassesDataContext context = new LeapDataClassesDataContext();
            List<ldc_results_election_candidate> candidate_list = GetCandidateList(context, state_fips, government_level_id);
            return View(candidate_list);
        }

        private Leap.Models.StateMatrix FindStateMatrix(LeapDataClassesDataContext context, int state_fips)
        {
            Leap.Models.StateMatrix matrix = new Models.StateMatrix();
            matrix.StateFips = state_fips;
            matrix.StateAbbr = context.list_loc_keys.Where(s => s.state_fips == state_fips).First().state_abbrev;

            matrix.FederalElectionCount = GetElectionCount(context, state_fips, 1);
            matrix.FederalPlaceCount = GetPlaceCount(context, state_fips, 1);
            matrix.FederalCandidateCount = GetCandidateCount(context, state_fips, 1);
            matrix.FederalYearRange = GetYearRange(context, state_fips, 1);

            matrix.StateElectionCount = GetElectionCount(context, state_fips, 2);
            matrix.StatePlaceCount = GetPlaceCount(context, state_fips, 2);
            matrix.StateCandidateCount = GetCandidateCount(context, state_fips, 2);
            matrix.StateYearRange = GetYearRange(context, state_fips, 2);

            matrix.CountyElectionCount = GetElectionCount(context, state_fips, 3);
            matrix.CountyPlaceCount = GetPlaceCount(context, state_fips, 3);
            matrix.CountyCandidateCount = GetCandidateCount(context, state_fips, 3);
            matrix.CountyYearRange = GetYearRange(context, state_fips, 3);

            matrix.CityElectionCount = GetElectionCount(context, state_fips, 4);
            matrix.CityPlaceCount = GetPlaceCount(context, state_fips, 4);
            matrix.CityCandidateCount = GetCandidateCount(context, state_fips, 4);
            matrix.CityYearRange = GetYearRange(context, state_fips, 4);

            matrix.SchoolElectionCount = GetElectionCount(context, state_fips, 5);
            matrix.SchoolPlaceCount = GetPlaceCount(context, state_fips, 5);
            matrix.SchoolCandidateCount = GetCandidateCount(context, state_fips, 5);
            matrix.SchoolYearRange = GetYearRange(context, state_fips, 5);

            matrix.SpecialElectionCount = GetElectionCount(context, state_fips, 6);
            matrix.SpecialPlaceCount = GetPlaceCount(context, state_fips, 6);
            matrix.SpecialCandidateCount = GetCandidateCount(context, state_fips, 6);
            matrix.SpecialYearRange = GetYearRange(context, state_fips, 6);

            /*matrix.JudicialElectionCount = GetElectionCount(context, state_fips, 7);
            matrix.JudicialPlaceCount = GetPlaceCount(context, state_fips, 7);
            matrix.JudicialCandidateCount = GetCandidateCount(context, state_fips, 7);
            matrix.JudicialYearRange = GetYearRange(context, state_fips, 7);*/

            return matrix;
        }

        // Get Election count & list
        private string GetElectionCount(LeapDataClassesDataContext context, int state_fips, int government_level_id)
        {
            IQueryable<ldc_results_election> query = GetElectionQuery(context, state_fips, government_level_id);
            var count_query = query.Select(e => new { e.election_date }).Distinct();
            return count_query.Count().ToString();
        }

        private List<ldc_results_election> GetElectionList(LeapDataClassesDataContext context, int state_fips, int government_level_id)
        {
            IQueryable<ldc_results_election> query = GetElectionQuery(context, state_fips, government_level_id);
            return query.OrderBy(e => e.election_date).ToList();
        }

        private IQueryable<ldc_results_election> GetElectionQuery(LeapDataClassesDataContext context, int state_fips, int government_level_id)
        {
            IQueryable<ldc_results_election> query = context.ldc_results_elections
                .Where(e => e.archive_flag == 0)
                .Where(e => e.state_fips == state_fips)
                .Where(e => e.government_level_id == government_level_id)
                .Where(e => e.election_date != null);
            string sql = context.GetCommand(query).CommandText;

            return query;
        }


        // Get Place count & list
        private string GetPlaceCount(LeapDataClassesDataContext context, int state_fips, int government_level_id)
        {
            IQueryable<ldc_results_election> query = GetPlaceQuery(context, state_fips, government_level_id);
            var count_query = query.Select(e => new { e.fips_place_location }).Distinct();
            return count_query.Count().ToString();
        }

        private List<ldc_results_election> GetPlaceList(LeapDataClassesDataContext context, int state_fips, int government_level_id)
        {
            IQueryable<ldc_results_election> query = GetPlaceQuery(context, state_fips, government_level_id);
            return query.OrderBy(e => e.fips_place_location).ToList();
        }

        private IQueryable<ldc_results_election> GetPlaceQuery(LeapDataClassesDataContext context, int state_fips, int government_level_id)
        {
            IQueryable<ldc_results_election> query = context.ldc_results_elections
                .Where(e => e.archive_flag == 0)
                .Where(e => e.state_fips == state_fips)
                .Where(e => e.government_level_id == government_level_id)
                .Where(e => e.fips_place_location != null);
            string sql = context.GetCommand(query).CommandText;
            return query;
        }


        // Get Candidate count & list
        private string GetCandidateCount(LeapDataClassesDataContext context, int state_fips, int government_level_id)
        {
            IQueryable<ldc_results_election_candidate> query = GetCandidateQuery(context, state_fips, government_level_id);
            var count_query = query.Select(c => new { c.results_candidate_id }).Distinct();
            return count_query.Count().ToString();
        }

        private List<ldc_results_election_candidate> GetCandidateList(LeapDataClassesDataContext context, int state_fips, int government_level_id)
        {
            IQueryable<ldc_results_election_candidate> query = GetCandidateQuery(context, state_fips, government_level_id);
            return query.OrderBy(c => c.results_candidate_id).ToList();
        }

        private IQueryable<ldc_results_election_candidate> GetCandidateQuery(LeapDataClassesDataContext context, int state_fips, int government_level_id)
        {
            IQueryable<ldc_results_election_candidate> query = context.ldc_results_election_candidates
                .Where(c => c.ldc_results_election.state_fips == state_fips)
                .Where(c => c.ldc_results_election.government_level_id == government_level_id)
                .Where(c => c.results_candidate_id != null);
            string sql = context.GetCommand(query).CommandText;
            return query;
        }


        private string GetYearRange(LeapDataClassesDataContext context, int state_fips, int government_level_id)
        {
            IQueryable<ldc_results_election> query = context.ldc_results_elections
                .Where(e => e.archive_flag == 0)
                .Where(e => e.state_fips == state_fips)
                .Where(e => e.government_level_id == government_level_id)
                .Where(e => e.election_date != null);

            var count_query = query
                .Select(e => e.election_date.Value.Year)
                .Distinct()
                .OrderBy(e => e);
            string sql = context.GetCommand(count_query).CommandText;

            List<int> year_list = count_query.ToList();

            return GenerateYearRangeStr(year_list);
        }

        public ActionResult CountyMatrix(string state, string county)
        {
            int state_fips = int.Parse(state);
            Leap.Models.CountyMatrix matrix = new Models.CountyMatrix();

            LeapDataClassesDataContext context = new LeapDataClassesDataContext();
            matrix.StateFips = state_fips;
            matrix.StateAbbr = context.list_loc_keys.Where(s => s.state_fips == state_fips).First().state_abbrev;
            if (county != "")
            {
                int county_fips = int.Parse(county);
                matrix.CountyFips = county_fips;
                matrix.CountyName = context.list_loc_keys.Where(c => c.state_fips == state_fips && c.county_fips == county_fips).First().county_name;
                matrix.ElectionCount = context.ldc_results_elections.Where(e => e.archive_flag == 0).Where(e => e.state_fips == state_fips && e.county_fips == county_fips).Where(e => e.data_log_id < 0).Distinct().Count().ToString();
                matrix.PlaceCount = context.ldc_results_elections.Where(e => e.archive_flag == 0).Where(e => e.state_fips == state_fips && e.county_fips == county_fips).Where(e => e.data_log_id < 0).Select(e => new { e.fips_place_location }).Distinct().Count().ToString();
                matrix.CandidateCount = context.ldc_results_election_candidates.Where(c => c.ldc_results_election.state_fips == state_fips && c.ldc_results_election.county_fips == county_fips).Where(c => c.ldc_results_election.data_log_id < 0).Distinct().Count().ToString();
                matrix.YearRange = GenerateYearRangeStr(context.ldc_results_elections.Where(e => e.archive_flag == 0).Where(e => e.state_fips == state_fips && e.county_fips == county_fips).Where(e => e.data_log_id < 0).Select(e => e.election_date.Value.Year).Distinct().ToList());
            }

            return View(matrix);
        }

        private string GenerateYearRangeStr(List<int> year_list)
        {
            if (year_list.Count == 0) return "None";
            if (year_list.Count == 1) return year_list[0] + " - " + year_list[0];

            int first_year = year_list[0];
            int last_year = year_list[year_list.Count - 1];
            string symbol = (last_year - first_year + 1 == year_list.Count) ? "" : "*";
            return first_year + " - " + last_year + symbol;
        }
                
        public ActionResult Find(string selected, string states)
        {
            XmlDocument outXml = new XmlDocument();
            String outfileName = Request.PhysicalPath + "/../../../Scripts/county_skeleton.xml";
            outXml.Load(outfileName);

            // append county placemarks of the selected state
            /*if (selected != "")
            {
                AppendCounties(int.Parse(selected), outXml);

                // extract this state from the string list
                states = states.Replace(selected + ",", "");
            }*/

            // append the placemarks of the given states except the selected
            //string[] state_fips_array = states.Split(',');
            List<int> state_fips_array = FindStatesWithData();
            AppendStates(state_fips_array, outXml);

            return new Leap.Models.XmlResult(outXml);
        }

        private List<int> FindStatesWithData()
        {
            LeapDataClassesDataContext context = new LeapDataClassesDataContext();

            List<int?> list = context.ldc_results_elections
                .Where(e => e.archive_flag == 0)
                .Where(e => e.leap_office_name_id > 0)
                .Where(e => e.meta_data_log.data_type >= 0)
                //.Where(e => e.government_level_id > 2)
                .Select(e => e.state_fips)
                .Distinct().ToList();

            List<int> fips_list = new List<int>();
            foreach (int fips in list)
            {
                if (fips == 0) continue;
                fips_list.Add(fips);
            }
            return fips_list;
        }

        private void AppendStates(List<int> state_fips_array, XmlDocument outXml)
        {
            XmlDocument state_xml = new XmlDocument();
            String fileName = Request.PhysicalPath + "/../../../Scripts/us_states.xml";
            state_xml.Load(fileName);

            foreach (int state_fips in state_fips_array)
            {
                //if (state_fips == "") continue;
                //AppendState(int.Parse(state_fips), state_xml, outXml);
                AppendState(state_fips, state_xml, outXml);
            }
        }

        private void AppendState(int state_fips, XmlDocument state_xml, XmlDocument outXml)
        {
            String state_fips_str = state_fips.ToString("00");
            XmlNode outFolderNode = outXml.SelectSingleNode("/kml/Document/Folder");

            foreach (XmlNode folderNode in state_xml.SelectNodes("/kml/Document"))
            {
                for (int xni = folderNode.SelectNodes("Placemark").Count - 1; xni >= 0; xni--)
                {
                    XmlNode xn = folderNode.SelectNodes("Placemark")[xni];
                    if (xn["fips"].InnerText == state_fips_str)
                    {
                        // create a node by copying the target
                        XmlNode copiedNode = outXml.ImportNode(xn, true);

                        // change the style
                        copiedNode.RemoveChild(copiedNode["styleUrl"]);
                        XmlElement style_url_element = outXml.CreateElement("styleUrl");
                        style_url_element.AppendChild(outXml.CreateTextNode("#Style_State"));
                        copiedNode.AppendChild(style_url_element);

                        // append the newly copied node
                        outFolderNode.AppendChild(copiedNode);
                        return;
                    }
                }
            }
        }
    }
}
