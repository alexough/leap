<xsl:stylesheet version="1.0" xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:user="urn:my-scripts" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet">
  <xsl:param name="include_inactive" select="''"/>
  <xsl:template match="/candidates">
		<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:html="http://www.w3.org/TR/REC-html40">
			<Styles>
				<Style ss:ID="Default" ss:Name="Normal">
					<Alignment ss:Vertical="Bottom"/>
					<Borders/>
					<Font/>
					<Interior/>
					<NumberFormat/>
					<Protection/>
				</Style>
				<Style ss:ID="s21">
					<Font ss:Bold="1"/>
					<Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
				</Style>
				<Style ss:ID="s22">
					<Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
					<Font ss:Bold="1"/>
					<Interior ss:Color="#99CCFF" ss:Pattern="Solid"/>
				</Style>
				<Style ss:ID="s23" ss:Name="Currency">
					<NumberFormat ss:Format="_(&quot;$&quot;* #,##0.00_);_(&quot;$&quot;* \(#,##0.00\);_(&quot;$&quot;* &quot;-&quot;??_);_(@_)"/>
				</Style>
				<Style ss:ID="s24">
					<NumberFormat ss:Format="#,###,##0;"/>
				</Style>
				<Style ss:ID="s25">
					<NumberFormat ss:Format="#,###,##0.00;"/>
				</Style>
				<Style ss:ID="s26">
					<Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
				</Style>
				<Style ss:ID="s27">
					<Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
					<Font ss:Bold="1"/>
				</Style>
				<Style ss:ID="s28">
					<Alignment ss:Horizontal="Right" ss:Vertical="Bottom"/>
				</Style>
        <Style ss:ID="ce4">
          <NumberFormat ss:Format="Short Date"/>
        </Style>
        <Style ss:ID="s29">
          <NumberFormat ss:Format="yyyy\-mm\-dd hh:MM:ss"/>
        </Style>
        <Style ss:ID="s30">
          <NumberFormat ss:Format="####"/>
        </Style>
      </Styles>
			<Worksheet>
				<xsl:attribute name="ss:Name"><xsl:value-of select="'Candidate'"/></xsl:attribute>
				<Table ss:ExpandedColumnCount="63">
					<Column ss:AutoFitWidth="0" ss:Width="50"/>
					<Column ss:AutoFitWidth="0" ss:Width="50"/>
					<Column ss:AutoFitWidth="0" ss:Width="50"/>
					<Column ss:AutoFitWidth="0" ss:Width="50"/>
					<Column ss:AutoFitWidth="0" ss:Width="50"/>
					<Column ss:AutoFitWidth="0" ss:Width="50"/>
					<Column ss:AutoFitWidth="0" ss:Width="50"/>
					<Column ss:AutoFitWidth="0" ss:Width="50"/>
					<Column ss:AutoFitWidth="0" ss:Width="50"/>
					<Column ss:AutoFitWidth="0" ss:Width="50"/>
					<Column ss:AutoFitWidth="0" ss:Width="50"/>
					<Column ss:AutoFitWidth="0" ss:Width="50"/>
					<Column ss:AutoFitWidth="0" ss:Width="50"/>
					<Column ss:AutoFitWidth="0" ss:Width="50"/>
					<Column ss:AutoFitWidth="0" ss:Width="50"/>
					<Column ss:AutoFitWidth="0" ss:Width="50"/>
					<Column ss:AutoFitWidth="0" ss:Width="50"/>
					<Column ss:AutoFitWidth="0" ss:Width="50"/>
					<Column ss:AutoFitWidth="0" ss:Width="50"/>
					<Column ss:AutoFitWidth="0" ss:Width="50"/>
					<Column ss:AutoFitWidth="0" ss:Width="50"/>
					<Column ss:AutoFitWidth="0" ss:Width="50"/>
					<Column ss:AutoFitWidth="0" ss:Width="50"/>
					<Column ss:AutoFitWidth="0" ss:Width="50"/>
					<Column ss:AutoFitWidth="0" ss:Width="50"/>
					<Column ss:AutoFitWidth="0" ss:Width="50"/>
					<Row>
            <Cell ss:StyleID="s21"><Data ss:Type="String">State Fips</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">County Fips</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Office Name</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Election Date</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Candidate Name</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Candidate Total Votes</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Candidate PCT</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Candidate Win</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">State Name</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">County Name</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Entity Name</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Entity Code</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Meta Data Name</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Fips Lookup Error</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Office AL</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Office District</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Seat Name</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Registered Votes</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Ballots Cast</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Timestamp</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Url</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Office Type Name</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Office Type Id</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Government Level Name</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Government Level Id</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Race Id</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Notes Count</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Page Date</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Election Name</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Leap Office Name</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Leap Office Name Id</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Election Type Desc</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Election Type Referendum</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Election Type Primary</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Election Type General</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Election Type Runoff</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Election Type Special</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Year</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Month</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Day</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Vote For</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Candidate First Name</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Candidate Middle Name</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Candidate Last Name</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Candidate Nick Name</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Candidate Name Suffix</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Candidate Party Id</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Candidate Name Type</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Candidate Referendum Type</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Candidate Incumbent</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Candidate Occupation</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Candidate PID</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Candidate Absent Votes</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Candidate Absent Pct</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Candidate Early Votes</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Candidate Early Pct</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Candidate Ed Votes</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Candidate Ed Pct</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Candidate Mail Votes</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Candidate Mail Pct</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Results Election Id</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Template Run Id</Data></Cell>
            <Cell ss:StyleID="s21"><Data ss:Type="String">Country Id</Data></Cell>
					</Row>
					<xsl:apply-templates select="candidate"/>
				</Table>
			</Worksheet>
		</Workbook>
	</xsl:template>


	<xsl:template match="candidate">
		<Row>
      <Cell ss:StyleID="s24"><Data ss:Type="Number"><xsl:value-of select="state_fips"/></Data></Cell>
      <Cell ss:StyleID="s24"><Data ss:Type="Number"><xsl:value-of select="county_fips"/></Data></Cell>
      <Cell><Data ss:Type="String"><xsl:value-of select="office_name"/></Data></Cell>
      <Cell ss:StyleID="ce4"><Data ss:Type="DateTime"><xsl:value-of select="election_date"/></Data></Cell>
      <Cell><Data ss:Type="String"><xsl:value-of select="candidate_name"/></Data></Cell>
      <Cell ss:StyleID="s24"><Data ss:Type="Number"><xsl:value-of select="candidate_total_votes"/></Data></Cell>
      <Cell ss:StyleID="s25"><Data ss:Type="Number"><xsl:value-of select="candidate_pct"/></Data></Cell>
      <Cell ss:StyleID="s24"><Data ss:Type="Number"><xsl:value-of select="candidate_win"/></Data></Cell>
      <Cell><Data ss:Type="String"><xsl:value-of select="state_abbrev"/></Data></Cell>
      <Cell><Data ss:Type="String"><xsl:value-of select="county_name"/></Data></Cell>
      <Cell><Data ss:Type="String"><xsl:value-of select="entity_name"/></Data></Cell>
      <Cell ss:StyleID="s30"><Data ss:Type="Number"><xsl:value-of select="entity_code"/></Data></Cell>
      <Cell><Data ss:Type="String"><xsl:value-of select="meta_data_name"/></Data></Cell>
      <Cell ss:StyleID="s30"><Data ss:Type="Number"><xsl:value-of select="fips_lookup_error"/></Data></Cell>
      <Cell><Data ss:Type="String"><xsl:value-of select="office_al"/></Data></Cell>
      <Cell><Data ss:Type="String"><xsl:value-of select="office_district"/></Data></Cell>
      <Cell><Data ss:Type="String"><xsl:value-of select="seat_name"/></Data></Cell>
      <Cell ss:StyleID="s24"><Data ss:Type="Number"><xsl:value-of select="registered_votes"/></Data></Cell>
      <Cell ss:StyleID="s24"><Data ss:Type="Number"><xsl:value-of select="ballots_cast"/></Data></Cell>
      <Cell ss:StyleID="s29"><Data ss:Type="DateTime"><xsl:value-of select="timestamp"/></Data></Cell>
      <Cell><Data ss:Type="String"><xsl:value-of select="url"/></Data></Cell>
      <Cell><Data ss:Type="String"><xsl:value-of select="office_type_name"/></Data></Cell>
      <Cell ss:StyleID="s24"><Data ss:Type="Number"><xsl:value-of select="office_type_id"/></Data></Cell>
      <Cell><Data ss:Type="String"><xsl:value-of select="government_level_name"/></Data></Cell>
      <Cell ss:StyleID="s24"><Data ss:Type="Number"><xsl:value-of select="government_level_id"/></Data></Cell>
      <Cell><Data ss:Type="String"><xsl:value-of select="race_id"/></Data></Cell>
      <Cell ss:StyleID="s24"><Data ss:Type="Number"><xsl:value-of select="notes_count"/></Data></Cell>
      <Cell ss:StyleID="ce4"><Data ss:Type="DateTime"><xsl:value-of select="page_date"/></Data></Cell>
      <Cell><Data ss:Type="String"><xsl:value-of select="election_name"/></Data></Cell>
      <Cell><Data ss:Type="String"><xsl:value-of select="leap_office_name"/></Data></Cell>
      <Cell ss:StyleID="s24"><Data ss:Type="Number"><xsl:value-of select="leap_office_name_id"/></Data></Cell>
      <Cell><Data ss:Type="String"><xsl:value-of select="election_type_desc"/></Data></Cell>
      <Cell><Data ss:Type="String"><xsl:value-of select="election_type_referendum"/></Data></Cell>
      <Cell><Data ss:Type="String"><xsl:value-of select="election_type_primary"/></Data></Cell>
      <Cell><Data ss:Type="String"><xsl:value-of select="election_type_general"/></Data></Cell>
      <Cell><Data ss:Type="String"><xsl:value-of select="election_type_runoff"/></Data></Cell>
      <Cell><Data ss:Type="String"><xsl:value-of select="election_type_special"/></Data></Cell>
      <Cell ss:StyleID="s30"><Data ss:Type="Number"><xsl:value-of select="year"/></Data></Cell>
      <Cell ss:StyleID="s24"><Data ss:Type="Number"><xsl:value-of select="month"/></Data></Cell>
      <Cell ss:StyleID="s24"><Data ss:Type="Number"><xsl:value-of select="day"/></Data></Cell>
      <Cell ss:StyleID="s24"><Data ss:Type="Number"><xsl:value-of select="vote_for"/></Data></Cell>
      <Cell><Data ss:Type="String"><xsl:value-of select="candidate_first_name"/></Data></Cell>
      <Cell><Data ss:Type="String"><xsl:value-of select="candidate_middle_name"/></Data></Cell>
      <Cell><Data ss:Type="String"><xsl:value-of select="candidate_last_name"/></Data></Cell>
      <Cell><Data ss:Type="String"><xsl:value-of select="candidate_nick_name"/></Data></Cell>
      <Cell><Data ss:Type="String"><xsl:value-of select="candidate_name_suffix"/></Data></Cell>
      <Cell ss:StyleID="s24"><Data ss:Type="Number"><xsl:value-of select="candidate_party_id"/></Data></Cell>
      <Cell ss:StyleID="s24"><Data ss:Type="Number"><xsl:value-of select="candidate_name_type"/></Data></Cell>
      <Cell ss:StyleID="s24"><Data ss:Type="Number"><xsl:value-of select="candidate_referendum_type"/></Data></Cell>
      <Cell ss:StyleID="s24"><Data ss:Type="Number"><xsl:value-of select="candidate_incumbent"/></Data></Cell>
      <Cell><Data ss:Type="String"><xsl:value-of select="candidate_occupation"/></Data></Cell>
      <Cell><Data ss:Type="String"><xsl:value-of select="candidate_pid"/></Data></Cell>
      <Cell ss:StyleID="s24"><Data ss:Type="Number"><xsl:value-of select="candidate_absent_votes"/></Data></Cell>
      <Cell ss:StyleID="s25"><Data ss:Type="Number"><xsl:value-of select="candidate_absent_pct"/></Data></Cell>
      <Cell ss:StyleID="s24"><Data ss:Type="Number"><xsl:value-of select="candidate_early_votes"/></Data></Cell>
      <Cell ss:StyleID="s25"><Data ss:Type="Number"><xsl:value-of select="candidate_early_pct"/></Data></Cell>
      <Cell ss:StyleID="s24"><Data ss:Type="Number"><xsl:value-of select="candidate_ed_votes"/></Data></Cell>
      <Cell ss:StyleID="s25"><Data ss:Type="Number"><xsl:value-of select="candidate_ed_pct"/></Data></Cell>
      <Cell ss:StyleID="s24"><Data ss:Type="Number"><xsl:value-of select="candidate_mail_votes"/></Data></Cell>
      <Cell ss:StyleID="s25"><Data ss:Type="Number"><xsl:value-of select="candidate_mail_pct"/></Data></Cell>
      <Cell ss:StyleID="s30"><Data ss:Type="Number"><xsl:value-of select="results_election_id"/></Data></Cell>
      <Cell ss:StyleID="s24"><Data ss:Type="Number"><xsl:value-of select="template_run_id"/></Data></Cell>
      <Cell ss:StyleID="s24"><Data ss:Type="Number"><xsl:value-of select="country_id"/></Data></Cell>
		</Row>
	</xsl:template>
</xsl:stylesheet>
