﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Leap.Models
{
    public class UserInfo
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string UserRole { get; set; }
    }
}