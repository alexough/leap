﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Leap.Models
{
    public class EventMonthArchive
    {
        public int Year { get; set; }
        public int Month { get; set; }
        public string MonthInStr { get; set; }
        public int Count { get; set; }
    }
}