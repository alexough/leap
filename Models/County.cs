﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Leap.Models
{
    public class County
    {
        public int? StateFips { get; set; }
        public string StateName { get; set; }
        public int? CountyFips { get; set; }
        public string CountyName { get; set; }
        public double? PrimaryLatitude { get; set; }
        public double? PrimaryLongitute { get; set; }
    }
}