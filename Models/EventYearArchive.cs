﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Leap.Models
{
    public class EventYearArchive
    {
        public int Year { get; set; }
        public int Count { get; set; }
        public List<EventMonthArchive> MonthArchiveList { get; set; }
    }
}