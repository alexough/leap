﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Leap.Models
{
    public class OfficeName
    {
        public int Id { get; set; }
        public String Name { get; set; }
        //public List<list_leap_office_name_synonym> Synonyms { get; set; }
        public List<String> Synonyms { get; set; }
    }
}
