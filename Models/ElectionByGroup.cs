﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Leap.Models
{
    public class ElectionByGroup
    {
        public String StateName { get; set; }
        public String CountyName { get; set; }
        public String LeapOfficeName { get; set; }
        public String ElectionDate { get; set; }
        public int ElectionCount { get; set; }
        public String TemplateRunDate { get; set; }
    }
}
