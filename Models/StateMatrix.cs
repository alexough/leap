﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Leap.Models
{
    public class StateMatrix
    {
        public int StateFips { get; set; }
        public string StateAbbr { get; set; }
        public String StateName { get; set; }
        public String FederalElectionCount { get; set; }
        public String FederalPlaceCount { get; set; }
        public String FederalCandidateCount { get; set; }
        public String FederalYearRange { get; set; }
        public String StateElectionCount { get; set; }
        public String StatePlaceCount { get; set; }
        public String StateCandidateCount { get; set; }
        public String StateYearRange { get; set; }
        public String CityElectionCount { get; set; }
        public String CityPlaceCount { get; set; }
        public String CityCandidateCount { get; set; }
        public String CityYearRange { get; set; }
        public String CountyElectionCount { get; set; }
        public String CountyPlaceCount { get; set; }
        public String CountyCandidateCount { get; set; }
        public String CountyYearRange { get; set; }
        public String SchoolElectionCount { get; set; }
        public String SchoolPlaceCount { get; set; }
        public String SchoolCandidateCount { get; set; }
        public String SchoolYearRange { get; set; }
        public String SpecialElectionCount { get; set; }
        public String SpecialPlaceCount { get; set; }
        public String SpecialCandidateCount { get; set; }
        public String SpecialYearRange { get; set; }
        /*public String JudicialElectionCount { get; set; }
        public String JudicialPlaceCount { get; set; }
        public String JudicialCandidateCount { get; set; }
        public String JudicialYearRange { get; set; }*/

    }
}