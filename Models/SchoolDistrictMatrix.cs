﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Leap.Models
{
    public class SchoolDistrictMatrix
    {
        public int StateFips { get; set; }
        public string StateAbbr { get; set; }
        public String StateName { get; set; }
        public int CountyFips { get; set; }
        public String CountyName { get; set; }
        public String CityName { get; set; }
        public int EntityCode { get; set; }
        public String EntityName { get; set; }
        public String ElectionCount { get; set; }
        public String PlaceCount { get; set; }
        public String CandidateCount { get; set; }
        public String YearRange { get; set; }
    }
}