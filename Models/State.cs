﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Leap.Models
{
    public class State
    {
        //public int StateId { get; set; }
        public int? CountryId { get; set; }
        public int? StateFips { get; set; }
        public string StateName { get; set; }
        public string StateAbbrev { get; set; }
    }
}