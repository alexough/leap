﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Leap.Helpers
{
    public class LeapLogger
    {
        public void Log(LeapDataClassesDataContext context, string user_name, string user_ip, string method, string message)
        {
            leap_log log = new leap_log();
            log.method = method;
            log.user_name = user_name;
            log.client_ip = user_ip;
            log.message = message;
            log.log_date = DateTime.Now;
            context.leap_logs.InsertOnSubmit(log);
            context.SubmitChanges();
        }

        public void LogPageEdit(LeapDataClassesDataContext context, string text_block_name, string user_name, string user_ip, string old_text, string new_text)
        {
            leap_page_edit_log log = new leap_page_edit_log();
            log.modified_by = user_name;
            log.client_ip = user_ip;
            log.text_block_name = text_block_name;
            log.old_text = old_text;
            log.new_text = new_text;
            log.modified_time = DateTime.Now;
            context.leap_page_edit_logs.InsertOnSubmit(log);
            context.SubmitChanges();
        }
    }
}
