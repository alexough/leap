﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Linq;

namespace Leap.Helpers
{
    public static class HtmlHelpers
    {
        public static string Truncate(this HtmlHelper helper, string input, int length)
        {
            if (input.Length <= length)
            {
                return input;
            }
            else
            {
                return input.Substring(0, length) + "...";
            }
        }

        public static string DisplayElectionType(this HtmlHelper helper, int? input)
        {
            if (input.HasValue)
            {
                if (input.Value == 0)
                {
                    return "no";
                }
                else
                {
                    return "yes";
                }
            }
            else
            {
                return "no";
            }
        }

        public static string DisplayDate(this HtmlHelper helper, System.DateTime? datetime)
        {
            if (datetime.HasValue)
            {
                return System.DateTime.Parse(datetime.ToString()).ToString("yyyy-MM-dd");
            }

            return "";
        }

        public static string DisplayDateInThreeLetterMonth(this HtmlHelper helper, System.DateTime? datetime)
        {
            if (datetime.HasValue)
            {
                return System.DateTime.Parse(datetime.ToString()).ToString("MMM dd, yyyy").ToUpper();
            }

            return "";
        }

        public static string DisplayDateTime(this HtmlHelper helper, System.DateTime? datetime)
        {
            if (datetime.HasValue)
            {
                return System.DateTime.Parse(datetime.ToString()).ToString("yyyy-MM-dd hh:mm:ss");
            }

            return "";
        }

        public static string DisplayYear(this HtmlHelper helper, System.DateTime? datetime)
        {
            if (datetime.HasValue)
            {
                return System.DateTime.Parse(datetime.ToString()).ToString("yyyy");
            }

            return "";
        }

        public static string DisplayMonthInThreeLeters(this HtmlHelper helper, System.DateTime? datetime)
        {
            if (datetime.HasValue)
            {
                return System.DateTime.Parse(datetime.ToString()).ToString("MMM").ToUpper();
            }

            return "";
        }

        public static string DisplayDayInTwoDigits(this HtmlHelper helper, System.DateTime? datetime)
        {
            if (datetime.HasValue)
            {
                return System.DateTime.Parse(datetime.ToString()).ToString("dd");
            }

            return "";
        }

        public static string DisplayId(this HtmlHelper helper, int? id)
        {
            if (!id.HasValue || id.Value == -1)
            {
                return "";
            }

            return id.ToString();
        }

        public static string DisplayEntityName(this HtmlHelper helper, int? stat_fips, int? county_fips, int? entity_code)
        {
            LeapDataClassesDataContext context = new LeapDataClassesDataContext();
            List<list_fips_meta_data> meta_data_list = context.list_fips_meta_datas
                .Where(m => m.state_fips == stat_fips)
                .Where(m => m.county_fips == county_fips)
                .Where(m => m.entity_code == entity_code).ToList();
            if (meta_data_list.Count == 0) return "";
            return meta_data_list[0].entity_name;
        }
    }
}