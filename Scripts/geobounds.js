
// Declare namespace
geobounds = window.marker || { instances: [] };

var after_parse_function = null;

// Constructor for the root KML parser object
geobounds.parser = function ()
{
    var parse = function (url, after_parse)
    {
        after_parse_function = after_parse;

        fetch_doc(url);
    };

    function fetch_doc(url)
    {
        geobounds.fetch_xml(url, function (response_xml) { render(response_xml); })
    }

    function processPlacemarkCoords(node, tag)
    {
        var parent = node.getElementsByTagName(tag);
        var coordListA = [];
        for (var i = 0; i < parent.length; i++)
        {
            var coordNodes = parent[i].getElementsByTagName('coordinates')
            if (!coordNodes)
            {
                if (coordListA.length > 0)
                {
                    break;
                } else
                {
                    return [{ coordinates: [] }];
                }
            }

            for (var j = 0; j < coordNodes.length; j++)
            {
                var coords = geoXML3.nodeValue(coordNodes[j]).trim();
                coords = coords.replace(/,\s+/g, ',');
                var path = coords.split(/\s+/g);
                var pathLength = path.length;
                var coordList = [];
                for (var k = 0; k < pathLength; k++)
                {
                    coords = path[k].split(',');
                    if (!isNaN(coords[0]) && !isNaN(coords[1]))
                    {
                        coordList.push({
                            lat: parseFloat(coords[1]),
                            lng: parseFloat(coords[0]),
                            alt: parseFloat(coords[2])
                        });
                    }
                }
                coordListA.push({ coordinates: coordList });
            }
        }
        return coordListA;
    }

    var render = function (responseXML)
    {
        var doc = {};
        //doc.internals = {};

        // Callback for retrieving a KML document: parse the KML and display it on the map
        if (!responseXML)
        {
            // Error retrieving the data
            geobounds.log('Unable to retrieve ');
        }
        else
        { //no errors
            doc.gpolygons = [];

            // Declare some helper functions in local scope for better performance
            var nodeValue = geobounds.nodeValue;

            var placemark, node, coords, path, marker, poly;
            var placemark, coords, path, pathLength, marker, polygonNodes, coordList;
            var placemarkNodes = responseXML.getElementsByTagName('Placemark');

            for (pm = 0; pm < placemarkNodes.length; pm++)
            {
                // Init the placemark object
                node = placemarkNodes[pm];
                var state_fips = null;
                var county_fips = null;
                if (node.getElementsByTagName('fips').length == 1)
                {
                    doc.state_fips = geobounds.nodeValue(node.getElementsByTagName('fips')[0]);
                }
                //var simple_data = {};
                /*for (var ei = 0; ei < node.getElementsByTagName('SimpleData').length; ei++)
                {
                    var element = node.getElementsByTagName('SimpleData')[ei];
                    if (element.getAttribute('name') == 'STATE')
                    {
                        state_fips = geobounds.nodeValue(element);
                    }
                    else if (element.getAttribute('name') == 'COUNTY')
                    {
                        county_fips = geobounds.nodeValue(element);
                    }
                }*/
                placemark = {
                    name: geobounds.nodeValue(node.getElementsByTagName('name')[0]),
                    //description: geobounds.nodeValue(node.getElementsByTagName('description')[0]),
                    state_fips: state_fips,
                    county_fips: county_fips,
                    styleUrl: geobounds.nodeValue(node.getElementsByTagName('styleUrl')[0]),
                    //simple_data: simple_data
                };

                // process MultiGeometry
                var GeometryNodes = node.getElementsByTagName('coordinates');
                var Geometry = null;
                if (!!GeometryNodes && (GeometryNodes.length > 0))
                {
                    for (var gn = 0; gn < GeometryNodes.length; gn++)
                    {
                        if (!GeometryNodes[gn].parentNode ||
                            !GeometryNodes[gn].parentNode.nodeName)
                        {

                        } else
                        { // parentNode.nodeName exists
                            var GeometryPN = GeometryNodes[gn].parentNode;
                            Geometry = GeometryPN.nodeName;

                            // Extract the coordinates
                            // What sort of placemark?
                            switch (Geometry)
                            {
                                case "Point":
                                    placemark.Point = processPlacemarkCoords(node, "Point")[0];
                                    placemark.latlng = new google.maps.LatLng(placemark.Point.coordinates[0].lat, placemark.Point.coordinates[0].lng);
                                    pathLength = 1;
                                    break;
                                case "LinearRing":
                                    // Polygon/line
                                    polygonNodes = node.getElementsByTagName('Polygon');
                                    // Polygon
                                    if (!placemark.Polygon)
                                        placemark.Polygon = [{
                                            outerBoundaryIs: { coordinates: [] },
                                            innerBoundaryIs: [{ coordinates: [] }]
                                        }];
                                    for (var pg = 0; pg < polygonNodes.length; pg++)
                                    {
                                        placemark.Polygon[pg] = {
                                            outerBoundaryIs: { coordinates: [] },
                                            innerBoundaryIs: [{ coordinates: [] }]
                                        }
                                        placemark.Polygon[pg].outerBoundaryIs = processPlacemarkCoords(polygonNodes[pg], "outerBoundaryIs");
                                        placemark.Polygon[pg].innerBoundaryIs = processPlacemarkCoords(polygonNodes[pg], "innerBoundaryIs");
                                    }
                                    coordList = placemark.Polygon[0].outerBoundaryIs;
                                    break;

                                case "LineString":
                                    pathLength = 0;
                                    placemark.LineString = processPlacemarkCoords(node, "LineString");
                                    break;

                                default:
                                    break;
                            }
                        } // parentNode.nodeName exists
                    } // GeometryNodes loop
                }

                if (placemark.Polygon)
                { // poly test 2
                    /*if (!!doc)
                    {
                        doc.gpolygons = doc.gpolygons || [];
                    }*/

                    poly = createPolygon(placemark, doc);
                    //poly.active = true;
                    doc.gpolygons.push(poly);

                    if (!!google.maps)
                    {
                        doc.bounds = doc.bounds || new google.maps.LatLngBounds();
                        doc.bounds.union(poly.bounds);
                    }
                }
            } // placemark loop

        }

        /*if (!!doc.bounds)
        {
            doc.internals.bounds = doc.internals.bounds || new google.maps.LatLngBounds();
            doc.internals.bounds.union(doc.bounds);
        }*/

        after_parse_function(doc);
    };

    // Create Polygon
    var createPolygon = function (placemark, doc)
    {
        var bounds = new google.maps.LatLngBounds();
        var pathsLength = 0;
        var paths = [];
        for (var polygonPart = 0; polygonPart < placemark.Polygon.length; polygonPart++)
        {
            for (var j = 0; j < placemark.Polygon[polygonPart].outerBoundaryIs.length; j++)
            {
                var coords = placemark.Polygon[polygonPart].outerBoundaryIs[j].coordinates;
                var path = [];
                for (var i = 0; i < coords.length; i++)
                {
                    var pt = new google.maps.LatLng(coords[i].lat, coords[i].lng);
                    path.push(pt);
                    bounds.extend(pt);
                }
                paths.push(path);
                pathsLength += path.length;
            }
            for (var j = 0; j < placemark.Polygon[polygonPart].innerBoundaryIs.length; j++)
            {
                var coords = placemark.Polygon[polygonPart].innerBoundaryIs[j].coordinates;
                var path = [];
                for (var i = 0; i < coords.length; i++)
                {
                    var pt = new google.maps.LatLng(coords[i].lat, coords[i].lng);
                    path.push(pt);
                    bounds.extend(pt);
                }
                paths.push(path);
                pathsLength += path.length;
            }
        }

        var polyOptions = geobounds.combineOptions(null, {
            //map:      parserOptions.map,
            paths: paths,
            title: placemark.name,
            /*strokeColor: kmlStrokeColor.color,
            strokeWeight: strokeWeight,
            strokeOpacity: kmlStrokeColor.opacity,
            fillColor: kmlFillColor.color,
            fillOpacity: kmlFillColor.opacity*/
        });
        var p = new google.maps.Polygon(polyOptions);
        p.bounds = bounds;

        //if (!!doc) doc.gpolygons.push(p);
        //placemark.polygon = p;
        return p;
    }

    return {
        // Expose some properties and methods
        parse: parse,
    };
};
// End of KML Parser

// Log a message to the debugging console, if one exists
geobounds.log = function (msg)
{
  if (!!window.console) {
    console.log(msg);
  } else { alert("log:"+msg); }
};

// Combine two options objects: a set of default values and a set of override values 
geobounds.combineOptions = function (overrides, defaults)
{
  var result = {};
  if (!!overrides) {
    for (var prop in overrides) {
      if (overrides.hasOwnProperty(prop)) {
        result[prop] = overrides[prop];
      }
    }
  }
  if (!!defaults) {
    for (prop in defaults) {
      if (defaults.hasOwnProperty(prop) && (result[prop] === undefined)) {
        result[prop] = defaults[prop];
      }
    }
  }
  return result;
};

// Retrieve an XML document from url and pass it to callback as a DOM document
geobounds.fetchers = [];

// parse text to XML doc
/**
 * Parses the given XML string and returns the parsed document in a
 * DOM data structure. This function will return an empty DOM node if
 * XML parsing is not supported in this browser.
 * @param {string} str XML string.
 * @return {Element|Document} DOM.
 */
geobounds.xml_parse = function (str)
{
  if (typeof ActiveXObject != 'undefined' && typeof GetObject != 'undefined') {
    var doc = new ActiveXObject('Microsoft.XMLDOM');
    doc.loadXML(str);
    return doc;
  }

  if (typeof DOMParser != 'undefined') {
    return (new DOMParser()).parseFromString(str, 'text/xml');
  }

  return createElement('div', null);
}

geobounds.fetch_xml = function (url, callback)
{
  function timeoutHandler() {
    callback();
  };

  var xhrFetcher = new Object();
  if (!!geobounds.fetchers.length)
  {
      xhrFetcher = geobounds.fetchers.pop();
  } else {
    if (!!window.XMLHttpRequest) {
      xhrFetcher.fetcher = new window.XMLHttpRequest(); // Most browsers
    } else if (!!window.ActiveXObject) {
      xhrFetcher.fetcher = new window.ActiveXObject('Microsoft.XMLHTTP'); // Some IE
    }
  }

  if (!xhrFetcher.fetcher) {
      geobounds.log('Unable to create XHR object');
    callback(null);
  } else {
      if (xhrFetcher.fetcher.overrideMimeType) {
        xhrFetcher.fetcher.overrideMimeType('text/xml');
      }
      xhrFetcher.fetcher.open('GET', url, true);
      xhrFetcher.fetcher.onreadystatechange = function () {
      if (xhrFetcher.fetcher.readyState === 4) {
        // Retrieval complete
        if (!!xhrFetcher.xhrtimeout)
          clearTimeout(xhrFetcher.xhrtimeout);
        if (xhrFetcher.fetcher.status >= 400) {
            geobounds.log('HTTP error ' + xhrFetcher.fetcher.status + ' retrieving ' + url);
          callback();
        } else {
          // Returned successfully
            callback(geobounds.xml_parse(xhrFetcher.fetcher.responseText));
        }
        // We're done with this fetcher object
        geobounds.fetchers.push(xhrFetcher);
      }
    };
    xhrFetcher.xhrtimeout = setTimeout(timeoutHandler, 60000);
    xhrFetcher.fetcher.send(null);
  }
};

//nodeValue: Extract the text value of a DOM node, with leading and trailing whitespace trimmed
geobounds.nodeValue = function (node)
{
  var retStr="";
  if (!node) {
    return '';
  }
   if(node.nodeType==3||node.nodeType==4||node.nodeType==2){
      retStr+=node.nodeValue;
   }else if(node.nodeType==1||node.nodeType==9||node.nodeType==11){
      for(var i=0;i<node.childNodes.length;++i){
         retStr+=arguments.callee(node.childNodes[i]);
      }
   }
   return retStr;
};
