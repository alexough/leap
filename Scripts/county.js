﻿
var geoXml = null;
var map = null;

/*var markerCluster = null;
var cluster_polygons = [];
var cluster_paths = [];
var cluster_bounds = [];
var markers = [];*/

var state_doc = null;
var state_list = {};
//var state_school_list = {};

var county_doc = null;
//var school_doc = null;

//var state_click_event_handlers = {};

//var dialog_script = new DialogScript();

var myLatLng = new google.maps.LatLng(39.50, -98.35);

var styles =
[
  {
    "featureType": "administrative.land_parcel",
    "stylers": [
      { "visibility": "off" }
    ]
  },{
    "featureType": "administrative.neighborhood",
    "stylers": [
      { "visibility": "simplified" }
    ]
  },{
    "featureType": "administrative.province",
    "stylers": [
      { "visibility": "off" }
    ]
  },{
    "featureType": "landscape.natural.landcover",
    "stylers": [
      { "visibility": "off" }
    ]
  },{
    "featureType": "landscape.natural.terrain",
    "stylers": [
      { "visibility": "off" }
    ]
  },{
    "featureType": "landscape.man_made",
    "stylers": [
      { "visibility": "off" }
    ]
  },{
    "featureType": "poi",
    "stylers": [
      { "visibility": "off" }
    ]
  },{
    "featureType": "road.local",
    "stylers": [
      { "visibility": "off" }
    ]
  },{
    "featureType": "transit.station",
    "stylers": [
      { "visibility": "off" }
    ]
  },{
    "featureType": "water",
    "elementType": "geometry.fill",
    "stylers": [
      { "visibility": "on" },
      { "color": "#3089e2" },
      { "lightness": 74 }
    ]
  },{
    "featureType": "landscape",
    "elementType": "geometry.fill",
    "stylers": [
      { "visibility": "on" },
      { "lightness": 83 },
      { "color": "#ffffff" }
    ]
  },{
    "featureType": "road",
    "elementType": "labels",
    "stylers": [
      { "visibility": "off" }
    ]
  },{
    "featureType": "road.highway",
    "stylers": [
      { "visibility": "off" }
    ]
  },{
    "featureType": "road.arterial",
    "elementType": "geometry",
    "stylers": [
      { "visibility": "off" }
    ]
  },{
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      { "visibility": "off" }
    ]
  },{
    "featureType": "road.arterial",
    "elementType": "geometry.stroke",
    "stylers": [
      { "visibility": "off" }
    ]
  },{
    "featureType": "transit",
    "stylers": [
      { "visibility": "off" }
    ]
  },{
    "featureType": "road.highway.controlled_access",
    "elementType": "geometry",
    "stylers": [
      { "visibility": "off" }
    ]
  },{
    "featureType": "road.highway",
    "elementType": "labels",
    "stylers": [
      { "visibility": "simplified" }
    ]
  },{
    "featureType": "landscape.man_made",
    "elementType": "geometry",
    "stylers": [
      { "visibility": "off" }
    ]
  },{
    "featureType": "road.highway.controlled_access",
    "stylers": [
      { "weight": 1.5 }
    ]
  },{
    "featureType": "administrative.neighborhood",
    "elementType": "labels",
    "stylers": [
      { "visibility": "on" }
    ]
  },{
    "featureType": "administrative",
    "elementType": "geometry.fill",
    "stylers": [
      { "visibility": "off" }
    ]
  },{
    "featureType": "administrative.neighborhood",
    "elementType": "labels",
    "stylers": [
      { "visibility": "simplified" }
    ]
  },{
    "featureType": "administrative.province",
    "elementType": "geometry.stroke",
    "stylers": [
      { "visibility": "on" },
      { "color": "#3182e4" }
    ]
  }
]
var myOptions = {
    center: myLatLng,
    zoom: 3,
    styles: styles,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    scrollwheel: false  // disable scrolling inside the map
};

var initial_state_style = {
    strokeColor: "#E73830",
    strokeWeight: 2,
    fillColor: "#E73830",
    fillOpacity: 0.40
};

var initial_style = {
    strokeColor: "#ff8888",
    strokeWeight: 2,
    fillColor: "#ff8888",
    fillOpacity: 0.40
};

var highlight_style = {
    fillColor: "#FFFF00",
    fillOpacity: 0.40,
    strokeColor: "#ff8888",
    strokeWeight: 2
};

var border_style = {
    fillColor: "#FFFFFF",
    fillOpacity: 0.00,
    strokeColor: '#ff8888',
    strokeOpacity: 1.0,
    strokeWeight: 2
};


var wait_message = "Please wait while initializing maps...";

function initialize()
{
    dialog_script.display_wait();

    var selected = "";
    var state_list_str = "46,6,12,21,1,4,22,37,51,";
    parse_state_xml(selected, state_list_str, true);
}

function parse_kml(kml_file_url, after_parse_function, latlng)
{
    dialog_script.display_wait(wait_message);

    if (latlng != null)
    {
        geoXml = new geoXML3.parser({
            //map: map,
            singleInfoWindow: true,
            afterParse: after_parse_function
        });
    }
    else
    {
        geoXml = new geoXML3.parser({
            map: map,
            singleInfoWindow: true,
            afterParse: after_parse_function
        });
    }

    geoXml.parse(kml_file_url, latlng);
}

function parse_only_kml(kml_file_url, parser, callback_function, latlng)
{
    dialog_script.display_wait(wait_message);

    doc = parser.parse(kml_file_url, callback_function);
}

function parse_state_xml(selected, state_list_str, refresh)
{
    if (refresh)
    {
        map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

        // Bounds for North America
        var strictBounds = new google.maps.LatLngBounds(
          //new google.maps.LatLng(28.70, -127.50),
          //new google.maps.LatLng(48.85, -55.90)
          new google.maps.LatLng(16.319648, -164.72168),
          new google.maps.LatLng(71.97203, -64.87793)
        );

        // Listen for the dragend event
        google.maps.event.addListener(map, 'dragend', function ()
        {
            if (strictBounds.contains(map.getCenter())) return;

            // We're out of bounds - Move the map back within the bounds

            var c = map.getCenter(),
                x = c.lng(),
                y = c.lat(),
                maxX = strictBounds.getNorthEast().lng(),
                maxY = strictBounds.getNorthEast().lat(),
                minX = strictBounds.getSouthWest().lng(),
                minY = strictBounds.getSouthWest().lat();

            if (x < minX) x = minX;
            if (x > maxX) x = maxX;
            if (y < minY) y = minY;
            if (y > maxY) y = maxY;

            map.setCenter(new google.maps.LatLng(y, x));
        });

        // restrict the min zoom level
        var minZoomLevel = 3;
        google.maps.event.addListener(map, "zoom_changed", function (oldLevel, newLevel)
        {
            var zoomLevel = map.getZoom();
            //map.setCenter(myLatLng);
            //alert('Zoom: ' + zoomLevel);
            if (zoomLevel < minZoomLevel) map.setZoom(minZoomLevel);
        });

        parse_kml('/Map/Find/?selected=' + selected + '&states=' + state_list_str + '&time=' + (new Date()).getTime(), register_state_data, null);
    }
    else
    {
        for (var idx = 0; idx < state_doc.gpolygons.length; idx++)
        {
            var state_fips = parseInt(state_doc.gpolygons[idx].placemark.state_fips, 10);
            if (state_fips == state_list_str)
            {
                state_doc.gpolygons[idx].setMap(map);
                //register_state_click_event(state_fips, state_doc.gpolygons[idx]);
                break;
            }
        }
    }
}

function parse_county_kml(state_fips)
{
    var state_fips = parseInt(state_fips, 10);

    parse_kml('/County/Find/?state_fips=' + state_fips + "&time=" + (new Date()).getTime(), register_county_data, null);
}

/*function randomColor()
{
    var color = "#";
    var colorNum = Math.random() * 8388607.0;
    // 8388607 = Math.pow(2,23)-1
    var colorStr = colorNum.toString(16);
    color += colorStr.substring(0, colorStr.indexOf('.'));
    return color;
};

function kmlColor(kmlIn)
{
    var kmlColor = {};
    if (kmlIn)
    {
        aa = kmlIn.substr(0, 2);
        bb = kmlIn.substr(2, 2);
        gg = kmlIn.substr(4, 2);
        rr = kmlIn.substr(6, 2);
        kmlColor.color = "#" + rr + gg + bb;
        kmlColor.opacity = parseInt(aa, 16) / 256;
    }
    else
    {
        // defaults
        kmlColor.color = randomColor();
        kmlColor.opacity = 0.45;
    }
    return kmlColor;
}*/

function register_county_data(doc)
{
    county_doc = doc[0];
    county_doc.state_fips = selected_state;

    for (var i = 0; i < county_doc.gpolygons.length; i++)
    {
        if (county_doc.placemarks[i]['has_data'] == 'True')
        {
            county_doc.gpolygons[i].setOptions(initial_style);
            register_county_click_event(county_doc.gpolygons[i], county_doc.placemarks[i]);
        }
        else
        {
            county_doc.gpolygons[i].setOptions(border_style);
        }
    }

    dialog_script.close_wait();
};

function register_county_click_event(poly, placemark)
{
    //var state_fips = parseInt(poly.placemark.simple_data.STATE, 10);
    var state_fips = parseInt(placemark.state_fips, 10);
    state_list[state_fips].push(poly);

    google.maps.event.addListener(poly, "click", function ()
    {
        for (var idx = 0; idx < state_list[state_fips].length; idx++)
        {
            state_list[state_fips][idx].setOptions(initial_style);
        }

        //highlightPoly(poly);
        poly.setOptions(highlight_style);

        if (selected_state != state_fips)
        {
            selected_state = state_fips;
            find_matrix(state_fips);
        }

        //var county_fips = parseInt(poly.placemark.simple_data.COUNTY, 10);
        var county_fips = parseInt(placemark.county_fips, 10);
        find_county_matrix(state_fips, county_fips);
    });
}

function register_state_data(doc)
{
    state_doc = doc[0];

    for (var i = 0; i < state_doc.gpolygons.length; i++)
    {
        var placemark = state_doc.placemarks[i];
        var state_fips = parseInt(placemark.state_fips, 10);
        state_list[state_fips] = [];
        //state_school_list[state_fips] = [];
        state_doc.gpolygons[i].placemark = placemark;
        state_doc.gpolygons[i].setOptions(initial_state_style);
        register_state_click_event(state_fips, state_doc.gpolygons[i]);
    }

    dialog_script.close_wait();
}

function register_state_click_event(state_fips, poly)
{
    var handler = google.maps.event.addListener(poly, "click", function (event)
    {
        selected_state_poly = poly;
        display_state_map(state_fips, poly, false);
    });
}

function display_state_map(state_fips, poly, refresh_state)
{
    // erase the map of this selected state
    poly.setMap(null);

    if (refresh_state || selected_state != state_fips)
    {
        if (refresh_state)
        {
            restore_state(null);
        }
        else if (selected_state != state_fips)
        {
            restore_state(selected_state);
        }
        find_matrix(state_fips);
    }

    selected_state = state_fips;

    /*if ($("#view_map_select").val() == "county")
    {
        // now draw the state counties
        parse_county_kml(state_fips);
        //find_matrix(state_fips);
        find_county_matrix(state_fips, "");
    }*/

    // now draw the state counties
    parse_county_kml(state_fips);
    find_county_matrix(state_fips, "");
}

function restore_state(state_to_restore)
{
    var prev_state_fips = null;

    if (county_doc != null)
    {
        for (var i = 0; i < county_doc.gpolygons.length; i++)
        {
            county_doc.gpolygons[i].setMap(null);
        }
        //prev_state_fips = parseInt(county_doc.state_fips, 10);
    }

    if (state_to_restore != null)
    {
        parse_state_xml("", state_to_restore, false);
    }
}
