﻿
function display_event_view() {
    $("div[id^='div_event_text_view_']").css("display", "inline");
    $("div[id^='div_event_text_edit_']").css("display", "none");
    $("#div_event_text_new").css("display", "none");
}

function display_event_edit(id) {
    /*$("div[id^='div_event_text_view_']").css("display", "none");
    $("div[id^='div_event_text_edit_']").css("display", "inline");
    $("#div_event_text_new").css("display", "none");*/
    $("#div_event_text_view_" + id).css("display", "none");
    $("#div_event_text_edit_" + id).css("display", "inline");
}

function find_all_events(num_to_fetch, div_obj, page, view_type) {
    var ajax_dict = {
        type: "GET",
        url: "/Event/FindAll/?page=" + page + "&view=" + view_type + "&num_to_fetch=" + num_to_fetch + "&i=" + (new Date()).getTime(),
        dataType: "html",
        processData: false,
        async: false,
        beforeSend: function () { find_all_events_before(); },
        complete: function (XMLHttpRequest) { find_all_events_complete(); },
        error: function (XMLHttpRequest, textStatus, errorThrown) { find_all_events_failed(XMLHttpRequest, textStatus, errorThrown); },
        success: function (result, textStatus) { find_all_events_succeeded(result, textStatus, div_obj, view_type); }
    }

    $.ajax(ajax_dict);
}

function find_all_events_before() {
    dialog_script.display_wait();
}

function find_all_events_complete() {
    dialog_script.close_wait();
}

function find_all_events_failed(XMLHttpRequest, textStatus, errorThrown) {
    alert(XMLHttpRequest.responseText);
}

function find_all_events_succeeded(result, textStatus, div_obj) {
    /*if (typeof (result['error']) != 'undefined') {
        div_obj.html('');
    }
    else {
        var div_str = '';
        for (var i = 0; i < result.length; i++) {
            div_str += render(view_type, result[i]['id'], result[i]['date'], result[i]['summary'], result[i]['detail']);
        }
        div_obj.html(div_str);
        display_event_view();
    }*/
    div_obj.html(result);
    display_event_view();
}





function find_event(id)
{
    var ajax_dict = {
        type: "GET",
        url: "/Event/Find/?id=" + id + "&i=" + (new Date()).getTime(),
        dataType: "json",
        //data: input_data,
        processData: false,
        //processData: true,
        async: false,
        beforeSend: function () { find_event_before(); },
        complete: function (XMLHttpRequest) { find_event_complete(); },
        error: function (XMLHttpRequest, textStatus, errorThrown) { find_event_failed(XMLHttpRequest, textStatus, errorThrown); },
        success: function (result, textStatus) { find_event_succeeded(result, textStatus, name); }
    }

    $.ajax(ajax_dict);
}

function find_event_before()
{
    dialog_script.display_wait();
}

function find_event_complete()
{
    dialog_script.close_wait();
}

function find_event_failed(XMLHttpRequest, textStatus, errorThrown)
{
    alert(XMLHttpRequest.responseText);
}

function find_event_succeeded(result, textStatus, name)
{
    if (typeof(result['error']) != 'undefined')
    {
        $("#" + name).html('');
    }
    else
    {




        $("#" + name).html(result['text_block']);
    }
}

/*function render(view_type, id, date, summary, detail)
{
    var month = 'MONTH';
    var day = '01';

    var str = "";
    str += '<div class="container-flud" style="width:100%">';
    //str += '<div id="div_event_text_view_1" eid="1" style="display:none">';
    str += '<div id="div_event_text_view_' + id + '" eid="' + id + '">';
    str += '<div class="date_box">' + month + '<br /><span class="month">' + day + '</span></div>';
    str += '<div class="date_desc">';
    str += summary;
    str += '<br />';
    str += '<a href="#" class="link">READ MORE</a>';
    str += '</div>';
    str += '</div>';
    str += '<div id="div_event_text_edit_' + id + '" eid="' + id + '" style="display:none">';
    str += '<input type="text" id="text_event_month_edit_' + id + '" eid="' + id + '" style="width:50px" value="' + month + '" />';
    str += '<input type="text" id="text_event_day_edit_' + id + '" eid="' + id + '" style="width:50px" value="' + day + '" />';
    str += '<textarea id="text_event_summary_edit_' + id + '" eid="' + id + '" style="width:400px;height:50px">';
    str += summary;
    str += '</textarea>';
    str += '<button id="ok_button_event_edit_text_' + id + '" eid="' + id + '" class="btn yellow" type="button" style="height:40px;">OK</button>';
    str += '<button id="cancel_button_event_edit_text_' + id + '" eid="' + id + '" class="btn yellow" type="button" style="height:40px;">Cancel</button>';
    str += '</div>';
    str += '</div>';
    str += '<br/>';

    return str;
}*/


function update_event(id, date, author, summary, detail, callback)
{
    var input_data = { 'id': id, 'date': date, 'author':author, 'summary': summary, 'detail': detail };

    var ajax_dict = {
        type: "POST",
        url: "/Event/Update/?i=" + (new Date()).getTime(),
        dataType: "json",
        data: input_data,
        //processData: false,
        processData: true,
        async: true,
        beforeSend: function () { update_event_before(); },
        complete: function (XMLHttpRequest) { update_event_complete(); },
        error: function (XMLHttpRequest, textStatus, errorThrown) { update_event_failed(XMLHttpRequest, textStatus, errorThrown); },
        success: function (result, textStatus) { update_event_succeeded(result, textStatus, callback); }
    }

    $.ajax(ajax_dict);
}

function update_event_before()
{
    dialog_script.display_wait();
}

function update_event_complete()
{
    dialog_script.close_wait();
}

function update_event_failed(XMLHttpRequest, textStatus, errorThrown)
{
    alert(XMLHttpRequest.responseText);
}

function update_event_succeeded(result, textStatus, callback)
{
    //alert(result);
    if (typeof callback != 'undefined' && callback != null) callback();
}
