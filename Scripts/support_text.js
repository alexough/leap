﻿
function init_text_edit(obj_name, view) {
    var text_obj_name = obj_name + "_text";
    var div_text_obj_name = "div_" + text_obj_name;
    var div_text_obj = $("#" + div_text_obj_name);
    var div_text_view_obj = $("#" + div_text_obj_name + "_view");
    var div_text_edit_obj = $("#" + div_text_obj_name + "_edit");
    var textarea_text_edit_obj = $("#text_" + text_obj_name + "_edit");
    var ok_button_obj = $("#ok_button_" + text_obj_name);
    var cancel_button_obj = $("#cancel_button_" + text_obj_name);

    find_text(div_text_obj_name);

    if (view == 'edit') {
        textarea_text_edit_obj.val(div_text_obj.html());
    }

    div_text_obj.click(function () {
        if (view != 'edit') return;
        textarea_text_edit_obj.val(div_text_obj.html());
        display_edit(div_text_view_obj, div_text_edit_obj);
    });

    ok_button_obj.click(function () {
        update_text(div_text_obj_name, textarea_text_edit_obj.val());
        div_text_obj.html(textarea_text_edit_obj.val());
        display_view(div_text_view_obj, div_text_edit_obj);
    });

    cancel_button_obj.click(function () {
        display_view(div_text_view_obj, div_text_edit_obj);
    });
}

function display_view(view_obj, edit_obj) {
    view_obj.css("display", "inline");
    edit_obj.css("display", "none");
}

function display_edit(view_obj, edit_obj) {
    view_obj.css("display", "none");
    edit_obj.css("display", "inline");
}

function find_text(name)
{
    var ajax_dict = {
        type: "GET",
        url: "/Manage/Find/?name=" + name + "&i=" + (new Date()).getTime(),
        dataType: "json",
        //data: input_data,
        processData: false,
        //processData: true,
        async: false,
        beforeSend: function () { find_text_before(); },
        complete: function (XMLHttpRequest) { find_text_complete(); },
        error: function (XMLHttpRequest, textStatus, errorThrown) { find_text_failed(XMLHttpRequest, textStatus, errorThrown); },
        success: function (result, textStatus) { find_text_succeeded(result, textStatus, name); }
    }

    $.ajax(ajax_dict);
}

function find_text_before()
{
    dialog_script.display_wait();
}

function find_text_complete()
{
    dialog_script.close_wait();
}

function find_text_failed(XMLHttpRequest, textStatus, errorThrown)
{
    alert(XMLHttpRequest.responseText);
}

function find_text_succeeded(result, textStatus, name)
{
    if (typeof(result['error']) != 'undefined')
    {
        $("#" + name).html('');
    }
    else
    {
        $("#" + name).html(result['text_block']);
    }
}


function update_text(name, text)
{
    var input_data = { 'name': name, 'text': text };

    var ajax_dict = {
        type: "POST",
        //url: "/Manage/Update/?name=" + name + "&text=" + text + "&i=" + (new Date()).getTime(),
        url: "/Manage/Update/?i=" + (new Date()).getTime(),
        dataType: "json",
        data: input_data,
        //processData: false,
        processData: true,
        async: true,
        beforeSend: function () { update_text_before(); },
        complete: function (XMLHttpRequest) { update_text_complete(); },
        error: function (XMLHttpRequest, textStatus, errorThrown) { update_text_failed(XMLHttpRequest, textStatus, errorThrown); },
        success: function (result, textStatus) { update_text_succeeded(result, textStatus); }
    }

    $.ajax(ajax_dict);
}

function update_text_before()
{
    dialog_script.display_wait();
}

function update_text_complete()
{
    dialog_script.close_wait();
}

function update_text_failed(XMLHttpRequest, textStatus, errorThrown)
{
    alert(XMLHttpRequest.responseText);
}

function update_text_succeeded(result, textStatus)
{
}


function open_window(url, target)
{
    window.open(url, target);
    return false;
}